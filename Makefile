OBJS = objects/DebugImage.o objects/Console.o objects/Random.o objects/IDMap.o
CS_OBJS = 
GAME_OBJS = 
SS_OBJS = 
UNUSED_OBJS =
DE_OBJS = SaveData.o Timer.o
DE_C_OBJS = $(addprefix ../DeltaEngine/objects/, $(DE_OBJS))

GEN_CLASSES = 	continent/Continent.cpp \
		continent/Generator.cpp \
		continent/Landform.cpp \
		continent/Biome.cpp \
		continent/generators/ScaledBase.cpp \
		continent/landforms/Crater.cpp \
		continent/landforms/River.cpp \
		continent/biomes/Plains.cpp
GEN_OBJECTS = $(GEN_CLASSES:.cpp=.o)

CC = g++ -std=c++0x -march=native
OLDCC = g++

DE = ../DeltaEngine/

debug: DEBUG = -g -O3 -DDEBUG

CFLAGS = -Wall  -Wextra -pipe -c $(DEBUG)
LFLAGS = -Wall  -Wextra -pipe $(DEBUG)

INCPATH = -I/usr/include/boost -I../DeltaEngine/ -I../DeltaEngine/lib/
# -Iext/qhull/src/

debug:   SDLFLAGS = -lpng -lz -ljpeg -lfreetype -lboost_system -lboost_filesystem -lgd -lnoise
release: SDLFLAGS = -Wl,-Bstatic -lpng -lz -ljpeg `freetype-config --cflags` -lfreetype -lm  -lboost_system -lboost_filesystem -lgd -lnoise  -Wl,-Bdynamic

LIBS = $(INCPATH) $(SDLFLAGS) -L/usr/include/boost
#ext/qhull/lib/libqhullstatic.a

debug : genTest.out
release : getTest.out

genTest.out : $(OBJS) $(GEN_OBJECTS) genTest.cpp
	$(CC) $(LFLAGS) $(LIBS) $(OBJS) $(GEN_OBJECTS) $(DE_C_OBJS) -O3 genTest.cpp  -o genTest.out
	
.cpp.o:
	$(CC) $(CFLAGS) $(INCPATH) $< -o $@
	
clean:
	rm $(GEN_OBJECTS)
	rm objects/*
	
objects/DebugImage.o: $(DE)lib/DebugImage.cpp $(DE)lib/DebugImage.h
	$(CC) $(CFLAGS) -O3 $(INCPATH) $(DE)lib/DebugImage.cpp -o objects/DebugImage.o
	
objects/Console.o: $(DE)lib/Console.cpp $(DE)lib/Console.h
	$(CC) $(CFLAGS) -O3 $(INCPATH) $(DE)lib/Console.cpp -o objects/Console.o
	
objects/Random.o: $(DE)lib/Random.cpp $(DE)lib/Random.h
	$(CC) $(CFLAGS) -O3 $(INCPATH) $(DE)lib/Random.cpp -o objects/Random.o
	
objects/IDMap.o: $(DE)lib/IDMap.cpp $(DE)lib/IDMap.h
	$(CC) $(CFLAGS) -O3 $(INCPATH) $(DE)lib/IDMap.cpp -o objects/IDMap.o