#pragma once
#include "include.h"

typedef unsigned char APType;

class ActorPersonality {
public:
  APType extroversion;//Postive is extroverted, negative is introverted
  APType intelligence;
  APType curiosity;
  APType idealism;
  
  APType stability;//Negative has strong emotional variance, postive a more stable personality
  APType outlook;//Negative tends to a negative outlook, positive- positive
  APType empathy;
  
  APType independence;
  APType dominance;
  APType bravery;
  APType liveliness;
  APType drive;
  
  APType vigilance;
  APType privateness
  APType purity;
};