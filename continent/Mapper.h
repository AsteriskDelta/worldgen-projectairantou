#pragma once
#include "include.h"

class Continent;
class DebugImage;

class Mapper {
public:
  Mapper(Continent *const c);
  ~Mapper();
  
  void SetResolution(Uint16 iWidth, Uint16 iHeight);
  
  bool Draw();
  bool Save(std::string path);
private:
  Continent *owner;
  Uint16 internalWidth, internalHeight;
  Uint16 totalWidth, totalHeight;
  Uint16 borderVertical, borderHorizontal;
  
  DebugImage *img;
}