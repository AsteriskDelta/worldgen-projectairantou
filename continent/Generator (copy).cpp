#define WORLDGEN_CPP

#include <noise/noise.h>
#include <iostream>
#include <sstream>
#include <cmath>

#include "lib/Random.h"
#include "lib/XMath.h"
#include "lib/DebugImage.h"
#include "generators/ScaledBase.h"

#include "Generator.h"

#define XOR_CONTINENT 	0x3fea29d6
#define XOR_MAGIC	0xa15e936c
#define XOR_VOLCANIC	0x329daf21

#define WATER_LEVEL 64

#define pow2(x) (pow((x), 2))

bool WorldGenObject::Generate(const WorldGenData& newData) {
  octaves = 15;
  
  data = newData;
  
  SetupContinent();
  SetupMagic();
  SetupVolcanos();
  
  DebugImage img(data.width, data.height);
  
  std::cout << "Generating base elevation and temperature data...\n";
  //Generate Stage I data
  worldMap = new WorldPixel*[data.width];
  for(int x = 0; x < data.width; x++) {
    worldMap[x] = new WorldPixel[data.height];
    for(int y = 0; y < data.height; y++) {
      WorldPixel *p = &worldMap[x][y];
      
      float xCoord = float(x)/data.scale, yCoord = float(y)/data.scale;
      
      int elevation = floor(255.0 * continentComplete->GetValue(xCoord, yCoord, 0.0));
      int lTemp = getBaseTemp(x, y, elevation);
      /*
      int magic = 0;//floor(255.0 * magicCurve->GetValue(xCoord, yCoord, 0.0));
      int volcanic = 0;//floor(255.0 * volcanicCurve->GetValue(xCoord, yCoord, 0.0));
      int tempurature = getBaseTemp(elevation);
      
      elevation = XMath::Clamp(elevation, 0, 255);
      magic = XMath::Clamp(magic, 0, 255);
      volcanic = XMath::Clamp(tempurature, 0, 255);*/
      
      p->elevation = elevation;
      p->lTemp = lTemp;
      //p->moisture
      //if(elevation < WATER_LEVEL) elevation = 0;
    }
  }
  
  //Simulate wind to generate base moisture and alter temperature
  std::cout << "simulating winds...\n";
  const Uint8 windVCount = 4;
  V2T<Int16> windVectors[windVCount];
  windVectors[0] = V2T<Int16>{ 1, 1};
  windVectors[1] = V2T<Int16>{-1,-1};
  windVectors[2] = V2T<Int16>{-1, 1};
  windVectors[3] = V2T<Int16>{ 1,-1};
  for(Uint8 v = 0; v < windVCount; v++) {
    V2T<Int16> vector = windVectors[v];
    //for()
  }
  
  std::cout << "creating world map...\n";
  for(int x = 0; x < data.width; x++) {
    for(int y = 0; y < data.height; y++) {
      WorldPixel *p = &worldMap[x][y];
      
      
      int drawElev = p->elevation;
      if(drawElev < WATER_LEVEL) drawElev = 0;
      int color = img.Color(0/*(p->lTemp / 10) + 60*/, drawElev, 0);
      img.Pixel(x, y, color);
    }
  }
  std::stringstream ss; ss << "height" << data.seed << ".png";
  img.Write(ss.str());
  return true;
}

int WorldGenObject::getBaseTemp(int x, int y, int elevation) {
  //x = round(((float)x / data.width) * data.width);
  //y = round(((float)y / data.height) * data.height);
  const float waterTempDistance = 12;
  float shoreModifier = 1.0f / ceil((float)std::max(elevation - WATER_LEVEL, 0) / waterTempDistance + 4.0f);//More temperamental close to water
  
  const int hCoordDT = sqrt(pow2(data.hTempX - x) + pow2(data.hTempY - y));
  const int lCoordDT = sqrt(pow2(data.lTempX - x) + pow2(data.lTempY - y));
  const float coordTempRatio = lCoordDT / (float)(hCoordDT + lCoordDT);
  float coordTemp = lerp(data.lTemp, data.hTemp, coordTempRatio);
  //std::cout << coordTemp <<":" << hCoordDT << "," << lCoordDT << " (" << x << ", " << y << ") \n";
  
  const int tempElevMin = 110;
  float elevModifier = std::max(0.0f, (float)elevation - tempElevMin) / (256.0f - tempElevMin) * 2.0f;
  
  float temp = 127.0f;//default tempurature
  temp = temp * shoreModifier;
  
  return round(lerp(coordTemp, (float)data.aTemp, shoreModifier));
}

void WorldGenObject::SetupContinent() {
  double continentFrequency = 0.1;
  
  //Continent Base
  continentBase = new noise::module::Perlin();
  continentBase->SetOctaveCount (octaves);
  continentBase->SetFrequency (0.1);
  continentBase->SetPersistence (0.6);
  continentBase->SetLacunarity(1.772);
  continentBase->SetSeed(data.seed ^ XOR_CONTINENT);
  
  continentScale = new noise::module::ScaledBase();
  continentScale->SetSourceModule(0, *continentBase);
  continentScale->SetCenter(double(data.width) / data.scale / 2.0, double(data.height) / data.scale / 2.0);
  continentScale->SetBases(-1.0, 0.6);
  continentScale->SetMods(0.0, 1.0);
  continentScale->SetRange(0.0, 1.0);
  
  //Noise
  continentNoiseRaw = new noise::module::Perlin();
  continentNoiseRaw->SetOctaveCount (octaves);
  continentNoiseRaw->SetFrequency (1.4);
  continentNoiseRaw->SetPersistence (0.6);
  continentNoiseRaw->SetLacunarity(1.272);
  continentNoiseRaw->SetSeed(data.seed ^ (XOR_CONTINENT - 1));
  
  continentNoiseScaled = new noise::module::ScaleBias();
  continentNoiseScaled->SetSourceModule (0, *continentNoiseRaw);
  continentNoiseScaled->SetScale (0.02);
  continentNoiseScaled->SetBias (0.015);
  
  //Carver
  continentCarver_raw = new noise::module::RidgedMulti();
  continentCarver_raw->SetOctaveCount (octaves);
  continentCarver_raw->SetFrequency(continentFrequency * 2.2);
  //continentCarver_raw->SetPersistence(0.6);
  continentCarver_raw->SetSeed(data.seed ^ (XOR_CONTINENT + 0xFF));
  
  continentCarver = new noise::module::ScaleBias();
  continentCarver->SetSourceModule (0, *continentCarver_raw);
  continentCarver->SetScale (-0.0);
  continentCarver->SetBias (0.8);
  
  continentCarved = new noise::module::Min();
  continentCarved->SetSourceModule (0, *continentScale);
  continentCarved->SetSourceModule (1, *continentCarver);
  
  //Apply noise
  continentPostNoise = new noise::module::Add();
  continentPostNoise->SetSourceModule (0, *continentCarved);
  continentPostNoise->SetSourceModule (1, *continentNoiseScaled);
  
  continentCurve = new noise::module::Curve();
  continentCurve->SetSourceModule(0, *continentPostNoise);
  continentCurve->AddControlPoint (0.0, 0.0);
  continentCurve->AddControlPoint (0.2, 0.2);
  continentCurve->AddControlPoint (0.4, 0.38);
  continentCurve->AddControlPoint (0.6, 0.52);
  continentCurve->AddControlPoint (0.8, 0.68);
  continentCurve->AddControlPoint (1.0, 1.0);
  
  continentComplete = new noise::module::Clamp();
  
  continentComplete->SetSourceModule (0, *continentCurve);
  continentComplete->SetBounds (-1.0, 1.0);
}

void WorldGenObject::SetupMagic() {
  //Magic
  magicBase = new noise::module::Billow();
  magicBase->SetSeed(data.seed ^ XOR_MAGIC);
  magicBase->SetFrequency (0.1);
  magicBase->SetPersistence (0.6);
  magicBase->SetOctaveCount (octaves);
  
  magicCurve = new noise::module::Curve();
  magicCurve->SetSourceModule(0, *magicBase);
  magicCurve->AddControlPoint(-1.0, 0.2);
  magicCurve->AddControlPoint(-0.8, 0.4);
  magicCurve->AddControlPoint( 0.0, 0.5);
  magicCurve->AddControlPoint( 0.6, 1.0);
  magicCurve->AddControlPoint( 0.8, 1.0);
  magicCurve->AddControlPoint( 1.0, 0.0);
}

void WorldGenObject::SetupVolcanos() {
  
  volcanicBase = new noise::module::RidgedMulti();
  volcanicBase->SetOctaveCount (octaves);
  volcanicBase->SetFrequency (0.05);
  //volcanicBase->SetPersistence (0.6);
  volcanicBase->SetLacunarity(2.25);
  volcanicBase->SetSeed(data.seed ^ XOR_VOLCANIC);
  
  volcanicCurve = new noise::module::Curve();
  volcanicCurve->SetSourceModule(0, *volcanicBase);
  volcanicCurve->AddControlPoint(-1.0, -1.0);
  volcanicCurve->AddControlPoint( 0.0, -1.0);
  volcanicCurve->AddControlPoint( 0.75, -1.0);
  volcanicCurve->AddControlPoint( 1.0, 1.0);
}