#pragma once
#include "include.h"

#define CBIOME_MAX 4096

class Continent; class Landform;
struct WorldPixel;

class Biome {
friend class Continent;
public:
  Biome();
  ~Biome();
  
  Uint16 id;
  
  //Weights inversly proportional to tightness of dimension requirement
  Uint8 elevationMin, elevationMax; float elevationWeight;
  Uint8 moistureMin, moistureMax; float moistureWeight;
  Uint8 auraMin, auraMax; float auraWeight;
  Int16 tempMin, tempMax; float tempWeight;
  Int8 ddeMin, ddeMax; float ddeWeight;
  
  Uint8 reqFlags;
  
  //Interface data
  std::string name;
  Color mapColor;
  
  void CalculateWeights();
  float GetScore(WorldPixel *const p);
  
  static void LoadAll();
  static void ActiveContinent(Continent * n);
  static Biome* Get(Uint16 id);
  static Biome* Classify(WorldPixel *const p);
protected:
  Uint8 elevationMid, moistureMid, auraMid;
  Int8 ddeMid;
  Int16 tempMid;
  
  static Continent *owner;
  static Biome biomes[CBIOME_MAX];
  static Uint16 biomeCount;
};