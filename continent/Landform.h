#pragma once
#include "include.h"

#define MAX_LANDFORMS 256
#define getRRange(min, max) ((GetRandom() % ((max) - (min))) + (min))

class DebugImage;
class Continent;

struct LandformInstance {
  V2T<Uint16> pos;
};

class Landform {
friend class Continent;
public:
  static void LoadAll();
  static void ActiveContinent(Continent *const n);
  static void AllPostBasic();
  static void AllPostWind();
  static void AllDebugDraw(DebugImage *img);
  
  static void SaveAll(std::ofstream *const fst);
  static void LoadAll(std::ifstream *const fst);
  
  Landform();
  ~Landform();
  
  //Universals
  virtual void SetOwner(Continent *const n);
  virtual void GenerateInstances();
  virtual void DeleteInstances();
  
  virtual void Save(std::ofstream *const fst);
  virtual void Load(std::ifstream *const fst);
  
  //Generation triggers
  virtual bool tPostBasic();
  virtual bool tPostWind();
  virtual void tDebugDraw(DebugImage *img);
  
  virtual Uint32 GetXOR();
  
  static Landform *landforms[MAX_LANDFORMS];
  static Uint16 landformCount;
protected:
  Continent* owner;
  
  Uint32 GetRandom();
  Uint32 randXor, randCnt;
};
