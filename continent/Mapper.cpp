#include "Mapper.h"
#include "Console.h"
#include "Continent.h"
#include "Landform.h"

Mapper::Mapper(Continent *const c) {
  img = NULL;
  owner = c;
  internalHeight = internalWidth = totalWidth = totalHeight = 0;
  borderVertical = borderHorizontal = 80;
}

Mapper::~Mapper() {
  if(img != NULL) delete img;
}

void Mapper::SetResolution(Uint16 iWidth, Uint16 iHeight) {
  internalWidth = iWidth; internalHeight = iHeight;
  totalWidth = internalWidth + borderHorizontal*2;
  totalHeight = internalHeight + borderVertical*2;
}

bool Mapper::Draw() {
  if(owner == NULL) {
    Console::Error("Attempted to draw NULL Continent!");
    return false;
  }
  img = new DebugImage(totalWidth, totalHeight);
  
  float sunTheta = DEG_TO_RAD * 45.f;
  
  //Draw map (pass 1)
  for(Uint16 x = 0; x < owner->omWidth; x++) {
    for(Uint16 y = 0; y < owner->omHeight; y++) {
      const Uint16 drawX = x + borderHorizontal, drawY = y + borderVertical;
      const WorldPixel *const p = &(owner->overmap[x][y]);
      V2T<float> dde = V2T<float>(p->dxde / WPDDE_SCALE, p->dyde / WPDDE_SCALE);
      V3T<float> normal = V3T<float>(-dde.x, 1.f, -dde.y).normalized();
      
      V3T<float> sunOrigin = V3T<float>(64000.f, 0.f, 0.f);
      
      
      const int color = img->Color(r, g, b);
      img->Pixel(drawX, drawY, color);
    }
  }
  
  return true;
}

bool Mapper::Save(std::string path) {
  if(img == NULL) {
    Console::Error("Attempted to save image prior to generation!");
    return false;
  }
  
  img->Write(path);
  return true;
}