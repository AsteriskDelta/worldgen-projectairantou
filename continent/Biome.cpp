#include "Biome.h"
#include "Continent.h"

Biome::Biome() {
  id = 0xFFFF;
}

Biome::~Biome() {
  
}

static const float pM = 1.f;
#define CWDIVW(x) (pM - x)
//(pM / ((x)))
void Biome::CalculateWeights() {
  elevationMid = (elevationMax - elevationMin)/2 + elevationMin;
  moistureMid = (moistureMax - moistureMin)/2 + moistureMin;
  auraMid = (auraMax - auraMin)/2 + auraMin;
  tempMid = (tempMax - tempMin)/2 + tempMin;
  ddeMid = (ddeMax - ddeMin)/2 + ddeMin;
  
  elevationWeight = float(elevationMid - elevationMin+1) * pM;
  moistureWeight = float(moistureMid - moistureMin+1) * pM;
  auraWeight = float(auraMid - auraMin+1) * pM;
  tempWeight = float(tempMid - tempMin+1) * pM;
  ddeWeight = float(ddeMid - ddeMin+1) * pM;
  
  /*const float tWeights = elevationWeight + moistureWeight + auraWeight + tempWeight + ddeWeight;
  elevationWeight = CWDIVW(elevationWeight);
  moistureWeight = CWDIVW(moistureWeight);
  auraWeight = CWDIVW(auraWeight);
  tempWeight = CWDIVW(tempWeight);
  ddeWeight = CWDIVW(ddeWeight);*/
  
  //std::cout << (float)moistureMin << " - " << (float)moistureMax << ", weight " << moistureWeight << "\n";
}

inline static float bgsCv(float in) {
  if(in > 1.f) {
    return 0.f;//in *= 1.3;
  }
  return pow(1.f - in, 1.f);
}

float Biome::GetScore(WorldPixel *const p) {//Lower is better
  if((p->flags ^ reqFlags) != 0x00) return 999999.f;
  /*else if(p->elevation < elevationMin || p->elevation > elevationMax ||
    p->moisture < moistureMin || p->moisture > moistureMax ||
    p->aura < auraMin || p->aura > auraMax ||
    p->temp < tempMin || p->temp > tempMax ||
    extreme(p->dxde, p->dyde) < ddeMin || extreme(p->dxde, p->dyde) > ddeMax) return 999999.f;*/
  
  float score = 1.f;
  score *= bgsCv(abs((float)p->elevation - (float)elevationMid) / elevationWeight);
  score *= bgsCv(abs((float)p->moisture - (float)moistureMid) / moistureWeight);
  score *= bgsCv(abs((float)p->aura - (float)auraMid) / auraWeight);
  score *= bgsCv(abs((float)p->temp - (float)tempMid) / tempWeight);
  score *= bgsCv(abs((float)extreme(p->dxde, p->dyde) - (float)ddeMid) / ddeWeight);
  //score /= 5.f;
  return 1.f-score;
}

/*
 * Static type functions
 */
Continent* Biome::owner = NULL;
Biome Biome::biomes[CBIOME_MAX];
Uint16 Biome::biomeCount = 0;

void Biome::LoadAll() {
  biomeCount = 0;
  Biome *b = &biomes[biomeCount];
  b->name = "Plains";
  b->mapColor = Color(175, 200, 76);
  b->elevationMin = 63; b->elevationMax = 250;
  b->moistureMin = 22; b->moistureMax = 85;
  b->auraMin = 0; b->auraMax = 255;
  b->tempMin = C_WPT(-50); b->tempMax = C_WPT(100);
  b->ddeMin = -127; b->ddeMax = 127;
  b->reqFlags = 0x00;
  biomeCount++;
  
  b = &biomes[biomeCount];
  b->name = "Forest";
  b->mapColor = Color(0, 128, 24);
  b->elevationMin = 60; b->elevationMax = 220;
  b->moistureMin = 65; b->moistureMax = 170;
  b->auraMin = 0; b->auraMax = 255;
  b->tempMin = C_WPT(-50); b->tempMax = C_WPT(100);
  b->ddeMin = -127; b->ddeMax = 127;
  b->reqFlags = 0x00;
  biomeCount++;
  
  b = &biomes[biomeCount];
  b->name = "Desert";
  b->mapColor = Color(189, 166, 103);
  b->elevationMin = 60; b->elevationMax = 220;
  b->moistureMin = 0; b->moistureMax = 32;
  b->auraMin = 0; b->auraMax = 255;
  b->tempMin = C_WPT(0); b->tempMax = C_WPT(100);
  b->ddeMin = -127; b->ddeMax = 127;
  b->reqFlags = 0x00;
  biomeCount++;
  
  b = &biomes[biomeCount];
  b->name = "Rainforest";
  b->mapColor = Color(10, 212, 20);
  b->elevationMin = 60; b->elevationMax = 220;
  b->moistureMin = 140; b->moistureMax = 248;
  b->auraMin = 0; b->auraMax = 255;
  b->tempMin = C_WPT(0); b->tempMax = C_WPT(100);
  b->ddeMin = -127; b->ddeMax = 127;
  b->reqFlags = 0x00;
  biomeCount++;
  
  for(Uint16 i = 0; i < biomeCount; i++) {
    Biome *const biome = &biomes[i];
    biome->id = i;
    biome->CalculateWeights();
  }
  
  std::stringstream ss; ss << "Loaded " << biomeCount << " biomes";
  Console::Print(ss.str());
}

void Biome::ActiveContinent(Continent * n) {
  owner = n;
}

Biome* Biome::Get(Uint16 id) {
  if(id > biomeCount) return NULL;
  return &(biomes[id]);
}

Biome* Biome::Classify(WorldPixel *const p) {
  float topScore[WPBIOME_BLENDCNT]; Uint16 topIndex[WPBIOME_BLENDCNT];
  for(Uint8 i = 0; i < WPBIOME_BLENDCNT; i++) { topScore[i] = 99999.f; topIndex[i] = 0xFFFF; };
  
  for(Uint16 i = 0; i < biomeCount; i++) {
    Biome *const biome = &biomes[i];
    float score = biome->GetScore(p);
    if(score >= 9999.f) continue;
    
    Uint8 scoreI = WPBIOME_BLENDCNT-1;
    while(score < topScore[scoreI]) {
      if(scoreI+1 < WPBIOME_BLENDCNT) {
	topScore[scoreI+1] = topScore[scoreI];
	topIndex[scoreI+1] = topIndex[scoreI];
      }
      
      topScore[scoreI] = score;
      topIndex[scoreI] = i;
      
      if(scoreI == 0) break;
      else scoreI--;
    }
  }
  
  //Set to positive being higher
  for(Uint16 i = 0; i < biomeCount; i++) {
    topScore[i] = 1.f - topScore[i];
  }
  
  float totalScore = 0.f; float maxScore = 0.f, minScore = 5.f;
  for(Uint8 i = 0; i < WPBIOME_BLENDCNT; i++) {
    if(topIndex[i] == 0xFFFF) continue;
    maxScore = std::max(topScore[i], maxScore);
    minScore = std::min(topScore[i], minScore);
    totalScore += topScore[i];
  }
  if(maxScore == 0.f) maxScore = 1.f;
  
  for(Uint8 i = 0; i < WPBIOME_BLENDCNT; i++) { 
    p->biome[i].id = topIndex[i];
    if(topIndex[i] != 0xFFFF) {
      float str = clamp((topScore[i]) / 1.f, 0.f, 1.f);
      //if(str == 1.f) str = 0.f;
      //else str = 1.f / str;
      p->biome[i].blend = (Uint8)round(WPBIOME_BSTRM * str);
      //std::cout << topScore[i] << ":" << topIndex[i] << " ";
    } else p->biome[i].blend = 0;
  }
  //if(totalScore != 0.f)std::cout << "|";
  return NULL;
}