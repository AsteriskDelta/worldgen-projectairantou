#pragma once
#include "shared/Shared.h"

struct WorldPixel {
  Uint8 elevation;
  
  Int16 lTemp, temp;
  Uint8 moisture;
  Uint8 aura;
};

struct WorldGenData {
  Uint16 width, height;//Width and height of feature map
  
  Int16 aTemp;
  int hTempX, hTempY, lTempX, lTempY;//tempurature extreme locations
  Int16 hTemp, lTemp;//Global latent high and low temperatures
  
  float delta, initial;//Fractal delta modifier (per subdivision) and initial delta
  float scale;
  Uint8 subdivisions;//Number of subdivisions to apply
  
  Uint32 seed;//Binary seed of the world
  
  Uint16 variance;
  
  Uint8 propagation;
  Uint8 startPoints;
};

class WorldGenObject {
public:
  bool Generate(const WorldGenData& newData);
  
  WorldPixel** worldMap;
private:
  int getBaseTemp(int x, int y, int elevation);
  
  Uint32 pCount;
  WorldGenData data;
#ifdef WORLDGEN_CPP
  noise::module::Perlin* continentBase;
  noise::module::Curve * continentCurve;
  noise::module::ScaledBase* continentScale;
  noise::module::RidgedMulti* continentCarver_raw;
  noise::module::ScaleBias* continentCarver;
  noise::module::Min* continentCarved;
  noise::module::Clamp* continentComplete;
  
  noise::module::Add* continentPostNoise;
  noise::module::Perlin* continentNoiseRaw;
  noise::module::ScaleBias* continentNoiseScaled;
  
  noise::module::Billow* magicBase;
  noise::module::Curve* magicCurve;
  
  noise::module::RidgedMulti* volcanicBase;
  noise::module::Curve* volcanicCurve;
#endif
  
  inline void SetupContinent();
  inline void SetupMagic();
  
  inline void SetupVolcanos();
  
  float* Xs;
  float* Ys;
  
  int octaves;
};