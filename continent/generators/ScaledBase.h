#pragma once

#include <noise/module/modulebase.h>

namespace noise {
  namespace module {
    class ScaledBase : public Module {
    public:
      ScaledBase();
      
      virtual int GetSourceModuleCount () const { return 1; };
      
      virtual double GetValue (double x, double y, double z) const;
      
      void SetCenter(double cX, double cY);
      void SetBases(double lBase, double uBase);
      void SetMods(double lMod, double uMod);
      void SetRange(double min, double max);
    protected:
      double centerX, centerY;
      
      double lowerBase, upperBase;
      double lowerMod, upperMod;
      
      double maxDist;
      
      int sources;
      
      double outMin, outMax;
      double range, offset;
    };
  };
};