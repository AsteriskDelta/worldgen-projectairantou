#define WORLDGEN_CPP

#include <noise/noise.h>
#include <iostream>
#include <sstream>
#include <cmath>

#include "lib/IDMap.h"
#include "lib/Console.h"
#include "lib/Random.h"
#include "lib/XMath.h"
#include "lib/DebugImage.h"
#include "generators/ScaledBase.h"

#include "Generator.h"
#include "Continent.h"
#include "Biome.h"

#define XOR_CONTINENT 	0x3fea29d6
#define XOR_MAGIC	0xa15e936c
#define XOR_VOLCANIC	0x329daf21

#define WATER_LEVEL 64

#define pow2(x) (pow((x), 2))

Generator::Generator(Continent *const newOwner) {
  owner = newOwner;
}

Generator::~Generator() {
  
}

bool Generator::Init() {
  
  return true;
}

bool Generator::GenerateBasics() {
  SetupContinent();
  SetupMagic();
  SetupVolcanos();
  
  WorldPixel *const *const data = owner->overmap;
  const V2T<float> scale = owner->GetGenScale();
  //std::cout << owner->GetOMScale().ToString() << "\n";
  const Uint16 omWidth = owner->omWidth, omHeight = owner->omHeight;
  
  Console::Progress("[%p] Generating world, %t", omWidth);
  Uint64 avgHeight = 0;
  for(Uint16 x = 0; x < omWidth; x++) {
    WorldPixel *const line = data[x];
    for(Uint16 y = 0; y < omHeight; y++) {
      WorldPixel *const p = &line[y];
      const float xCoord = float(x) / scale.x, yCoord = float(y) / scale.y;
      
      p->elevation = (Uint8)floor(255.0 * continentComplete->GetValue(xCoord, yCoord, 0.0));
      owner->SetBaseTempWind(V2T<Uint16>(x, y));//getBaseTemp(x, y, elevation);
      p->moisture = (p->elevation < owner->omSeaLevel) ? 255 : 0;
      p->aura = floor(255.0 * magicCurve->GetValue(xCoord, yCoord, 0.0));
      avgHeight += p->elevation;
      
      p->flags = 0x00;
      p->waterState = WPWS_NA;
      p->processed = false;
      /*
       *   
       *   int volcanic = 0;//floor(255.0 * volcanicCurve->GetValue(xCoord, yCoord, 0.0));
       *   int tempurature = getBaseTemp(elevation);
       *   
       *   elevation = XMath::Clamp(elevation, 0, 255);
       *   magic = XMath::Clamp(magic, 0, 255);
       *   volcanic = XMath::Clamp(tempurature, 0, 255);*/
      //p->moisture
      //if(elevation < WATER_LEVEL) elevation = 0;
    }
    Console::Progress(x);
  }
  Console::Progress(owner->omWidth);
  //owner->omAverageElevation = float(avgHeight / (omWidth*omHeight));
  return true;
}
/*
bool Generator::SimulateWind(Uint16 iterMax) {
  const Uint8 seaLevel = owner->omSeaLevel, seaThresh = (int)seaLevel * 3 / 22;
  const Int16 omWidth = Int16(owner->omWidth) - 2;
  const Int16 omHeight = Int16(owner->omHeight) - 2;
  const V2T<float> scale = owner->GetOMScale();
  
  Console::Progress("[%p] Simulating winds and propagating moisture, line %e, %t", iterMax * omWidth);
  for(Uint16 i = 0; i < iterMax; i++) {
    for(Int16 x = 2; x < omWidth; x++) {
      for(Int16 y = 2; y < omHeight; y++) {
	WorldPixel *const sp = &(owner->overmap[x][y]);
	if(sp->elevation < seaThresh) continue;
	
	float moisture = .5f * float(sp->moisture);
	V2T<float> vec = V2T<float>(sp->wind.x, sp->wind.y).normalized();
	V2T<float> dir = vec; float dist = 0.f;
	V2T<float> pos = V2T<float>(x, y);
	
	while(moisture > 1.f && pos.x > 2 && pos.x < omWidth && pos.y > 2 && pos.y < omHeight && vec.magnitude() > .5f) {
	  float speed = vec.magnitude();
	  dir += vec; dist += speed;
	  WorldPixel *const o = &(owner->overmap[(Uint16)roundf(pos.x)][(Uint16)roundf(pos.y)]);
	  float distMult = (vec*2.f*scale).magnitude();
	  
	  //Alter moisture, transfer to winds or land
	  float oMoisture = float(o->moisture);
	  float mTransfer = oMoisture - moisture;//positive to wind, negative to ground
	  mTransfer *= clamp((o->elevation - owner->omSeaLevel) * mTransMod *0.6f + .4f, 0.f, 1.f);//Higher altitude = higher transfer rate
	  mTransfer *= distMult;
	  moisture += mTransfer * 0.5f;
	  
	  if(seaLevel <= o->elevation) {//Alter based on elevation
	    oMoisture -= mTransfer;
	    if(o->moisture != 255) o->moisture = (Uint8)clamp(Uint8(oMoisture), (Uint8)0, (Uint8)255);
	    
	    V2T<float> pEVector = V2T<float>(o->dxde, o->dyde) * scale / -WPDDE_SCALE;
	    V2T<float> wEVector = vec.normalized() * scale;
	    float cross = pEVector.x * wEVector.y - pEVector.y * wEVector.x;
	    //Positive: vector runs to the right of elevationPlus, negative to the left
	    
	    if(cross < 0.f) {//Skew right, 90-deg rotation
	      pEVector = V2T<float>(-pEVector.y,  pEVector.x);
	    } else if(cross > 0.f) {//Skew left, 270-deg rotation
	      pEVector = V2T<float>( pEVector.y, -pEVector.x);
	    } else {//Reduce magnitiude
	      pEVector = vec * -1.f;
	    }
	    const float vlScale = clamp(distMult * speed / 36.f , 0.f, 1.f);
	    vec = lerp(vec, pEVector, vlScale ).normalized();
	  }
	  
	  pos += vec;
	}
	
	//Update wind vector to average dir over distance
	dir /= dist;
	dir = dir.normalized() * WPWIND_VMULT;// * clamp(moisture/234.f, 0.177f, 1.f);
	sp->wind = WPWind{(Int8)(dir.x), (Int8)(dir.y)};
      }
      Console::Progress(i * omWidth + x);
    }
  }
  Console::Progress(iterMax * omWidth);
  
  return true;
}
*/

bool Generator::SimulateWind(Uint16 iterMax) {
  const Uint8 seaLevel = owner->omSeaLevel, seaThresh = (int)seaLevel * 3 / 22;
  const Int16 ovWidth = Int16(owner->ovWidth) - 2;
  const Int16 ovHeight = Int16(owner->ovHeight) - 2;
  const float mTransMod = (1.f/(255.f - owner->omSeaLevel));// * .6f / owner->GetOMScale().y;
  //const float mFalloffConstant = 1.f - .01f;//pow(clamp(owner->GetOMScale().x * .66f, .2f, .999f), 8);
  //const float mDivConstant = owner->GetOMScale().x*1.2f;
  const V2T<float> scale = owner->GetOMScale();
  
  Console::Progress("[%p] Simulating winds and propagating moisture, line %e, %t", iterMax * ovWidth);
  for(Uint16 i = 0; i < iterMax; i++) {
    for(Int16 x = 1; x < ovWidth; x++) {
      for(Int16 y = 1; y < ovHeight; y++) {
	float kmX = float(owner->width  * ((float)x / (float)owner->ovWidth ));
	float kmY = float(owner->height * ((float)y / (float)owner->ovHeight));
	const WorldPixel sp = owner->GetOMPixel(kmX, kmY);
	//const WorldVexel vp = owner->GetVexel(kmX, kmY);
	WorldVexel *const vp = &(owner->vexelmap[x][y]);
	if(sp.elevation < seaThresh) continue;
	
	float moisture = .5f * float(sp.moisture);
	V2T<float> vec = V2T<float>(vp->wind.x, vp->wind.y).normalized();
	V2T<float> dir = vec; float dist = 0.f;
	V2T<float> pos = V2T<float>(x, y);
	
	while(moisture > 1.f && pos.x > 1 && pos.x < ovWidth && pos.y > 1 && pos.y < ovHeight && vec.magnitude() > .1f) {
	  //std::cout << moisture << "\n";
	  float speed = vec.magnitude();
	  dir += vec; dist += speed;
	  //WorldPixel *const o = &(owner->overmap[(Uint16)roundf(pos.x)][(Uint16)roundf(pos.y)]);
	  float okmX = float(owner->width  * ((float)pos.x / (float)owner->ovWidth ));
	  float okmY = float(owner->height * ((float)pos.y / (float)owner->ovHeight));
	  const WorldPixel o = owner->GetOMPixel(okmX, okmY);
	  WorldVexel *const ovp = &(owner->vexelmap[(int)round(pos.x)][(int)round(pos.y)]);
	  float distMult = 1.f;//(vec*2.f*scale).magnitude();
	  
	  //Alter moisture, transfer to winds or land
	  float oMoisture = float(o.moisture);
	  float mTransfer = oMoisture - moisture;//positive to wind, negative to ground
	  mTransfer *= clamp((o.elevation - owner->omSeaLevel) * mTransMod *0.6f + .4f, 0.f, 1.f);//Higher altitude = higher transfer rate
	  mTransfer *= distMult;
	  moisture += mTransfer * 0.5f;
	  
	  if(seaLevel <= o.elevation) {//Alter based on elevation
	    //oMoisture -= mTransfer;
	    if(o.moisture != 255) ovp->pMoisture -= mTransfer;//o->moisture = (Uint8)clamp(Uint8(oMoisture), (Uint8)0, (Uint8)255);
	    
	    V2T<float> pEVector = V2T<float>(o.dxde, o.dyde) * scale / -WPDDE_SCALE;
	    V2T<float> wEVector = vec.normalized() * scale;
	    float cross = pEVector.x * wEVector.y - pEVector.y * wEVector.x;
	    //Positive: vector runs to the right of elevationPlus, negative to the left
	    
	    /*if(cross < 0.f) {//Skew right, 90-deg rotation
	      pEVector = V2T<float>(-pEVector.y,  pEVector.x);
	    } else if(cross > 0.f) {//Skew left, 270-deg rotation
	      pEVector = V2T<float>( pEVector.y, -pEVector.x);
	    } else {//Reduce magnitiude
	      pEVector = vec * -1.f;
	    }
	    const float vlScale = clamp(distMult * speed / 36.f , 0.f, 1.f);
	    vec = lerp(vec, pEVector, vlScale ).normalized();*/
	    //vec += vec*vlScale;
	    
	    //Alter wind vector by elevation derivative
	    //V2T<float> cross = V2T<float>(vec.y, -vec.x);
	    //cross = cross.normalized();
	    //float rHeight = owner->overmap[(Uint16)(pos.x+cross.x)][(Uint16)(pos.y+cross.y)].elevation - o->elevation;
	    //float lHeight = owner->overmap[(Uint16)(pos.x-cross.x)][(Uint16)(pos.y-cross.y)].elevation - o->elevation;
	    
	    //R > L : skew > 0, R < L: skew < 0
	    //NOTE: THIS SHOULD BE REDONE!!!
	    //float skew = clamp((lHeight - rHeight) / 42.f, -1.f, 1.f) / (13.f / mDivConstant) * 0.6f;
	    //if(skew > 0.f) vec = lerp(vec, cross, skew);
	    //else if(skew < 0.f) vec = lerp(vec, cross * -1.f, -skew);
	  }
	  
	  pos += vec;
	  if(mTransfer < 0.f) moisture += -mTransfer * distMult * 0.3f;
	}
	
	//Update wind vector to average dir over distance
	dir /= dist;
	dir = dir.normalized() * WPWIND_VMULT;// * clamp(moisture/234.f, 0.177f, 1.f);
	vp->wind = WPWind{(Int8)(dir.x), (Int8)(dir.y)};
      }
      Console::Progress(i * ovWidth + x);
    }
  }
  
  int xVInc = owner->omWidth / owner->ovWidth, yVInc = owner->omHeight / owner->ovHeight;
  Uint16 omWidth = owner->omWidth - 1 - xVInc, omHeight = owner->omHeight - 1 -yVInc;
  /*
  for(Int16 x = xVInc + 1; x < omWidth; x++) {
    for(Int16 y = yVInc+1; y < omHeight; y++) {
      float kmX = float(owner->width  * ((float)x / (float)owner->omWidth ));
      float kmY = float(owner->height * ((float)y / (float)owner->omHeight));
      if(kmX > owner->width || kmY > owner->height) {
	std::cout << "kmX:" << kmX << " kmY:" << kmY << " at " << x << ", " << y << "\n";
      }
      WorldVexel v = owner->GetOMVexel(kmX, kmY); 
      WorldPixel *const p = &(owner->overmap[x][y]);
      p->moisture = clamp(p->moisture + v.pMoisture, 0, 253);
    }
  }*/
  
  Console::Progress(iterMax * ovWidth);
  
  return true;
}

static const Uint8 kSize = 15, kHalf = 7, kQuarter = 3;
static const float gKernel[kSize] = {0.023089, 0.034587, 0.048689, 0.064408, 0.080066, 0.093531, 0.102673, 0.105915, 0.102673, 0.093531, 0.080066, 0.064408, 0.048689, 0.034587, 0.023089};

bool Generator::BlurMoisture(Uint16 iters) {
  const V2T<Uint16> minLand = owner->omMinLand, maxLand = owner->omMaxLand;
  
  for(Uint16 i = 0; i < iters; i++) {
    for(Uint16 x = minLand.x; x < maxLand.x; x++) {//Copy to double buffer
      for(Uint16 y = minLand.y; y < maxLand.y; y++) {
	owner->omTemp[x][y].moisture = owner->overmap[x][y].moisture;
      }
    }
    
    for(Uint16 x = minLand.x; x < maxLand.x; x++) {//X-rel blur
      for(Uint16 y = minLand.y; y < maxLand.y; y++) {
	if(owner->overmap[x][y].moisture == 255) continue;
	
	const Uint16 kMax = x + kHalf ; float v = .0f;
	Uint8 kernelOffset = 0;
	for(Uint16 k = x - kHalf ; k < kMax; k++) {
	  const WorldPixelTMP *const p = &owner->omTemp[k][y];
	  v += gKernel[kernelOffset] * float(p->moisture);
	  kernelOffset++;
	}
	owner->overmap[x][y].moisture = (Uint8)round(v);
      }
    }
    
    for(Uint16 x = minLand.x; x < maxLand.x; x++) {//Copy to double buffer
      for(Uint16 y = minLand.y; y < maxLand.y; y++) {
	owner->omTemp[x][y].moisture = owner->overmap[x][y].moisture;
      }
    }
    
    for(Uint16 x = minLand.x; x < maxLand.x; x++) {//Y-rel
      for(Uint16 y = minLand.y; y < maxLand.y; y++) {
	if(owner->overmap[x][y].moisture == 255) continue;
	
	const Uint16 kMax = y + kHalf ; float v = .0f;
	Uint8 kernelOffset = 0;
	for(Uint16 k = y - kHalf ; k < kMax; k++) {
	  const WorldPixelTMP *const p = &owner->omTemp[x][k];
	  v += gKernel[kernelOffset] * float(p->moisture);
	  kernelOffset++;
	}
	owner->overmap[x][y].moisture = (Uint8)round(v);
      }
    }
  }
  
  return true;
}

static IDMap<float> bbBiomeIDs(64);
bool Generator::BlurBiomes(Uint16 iters) {
  const V2T<Uint16> minLand = owner->omMinLand, maxLand = owner->omMaxLand;
  Console::Progress("[%p] Smoothing biomes...", (maxLand.x - minLand.x)*iters*2);
  unsigned int progress = 0;
  
  for(Uint16 i = 0; i < iters; i++) {
    //Copy to double-buffer temp struct
    for(Uint16 x = 1; x < owner->omWidth-1; x++) {
      for(Uint16 y = 1; y < owner->omHeight-1; y++) {
	const WorldPixel *const sq = &owner->overmap[x][y];
	WorldPixelTMP *const tsq = &owner->omTemp[x][y];
	if(x >= minLand.x && x < maxLand.x && y >= minLand.y && y < maxLand.y) {
	  for(Uint8 b = 0; b < WPBIOME_BLENDCNT; b++) {
	    tsq->biome[i].id = sq->biome[i].id;
	    tsq->biome[i].blend = sq->biome[i].blend;
	  }
	} else {
	  for(Uint8 b = 0; b < WPBIOME_BLENDCNT; b++) {
	    tsq->biome[i].id = WPBIOME_NULLID;
	    tsq->biome[i].blend = 0;
	  }
	}
      }
    }
    
    for(Uint16 x = minLand.x; x < maxLand.x; x++) {//X-rel blur
      for(Uint16 y = minLand.y; y < maxLand.y; y++) {
	if(owner->overmap[x][y].biome[0].id == WPBIOME_NULLID) continue;
	//std::cout << x << ", " << y << "\n";
	const Uint16 kMax = x + kQuarter ; Uint8 kernelOffset = 0;
	WorldPixel *const sq = &owner->overmap[x][y];
	
	bbBiomeIDs.Clear();
	for(Uint16 k = x - kQuarter; k <= kMax; k++) {
	  const WorldPixelTMP *const p = &owner->omTemp[k][y];
	  //std::cout << "\t" << k << ", " << y << "\n";
	  for(Uint8 b = 0; b < WPBIOME_BLENDCNT; b++) {
	    //std::cout << k << ", " << y << "@" << ((int)b) <<": \n";
	    if(p->biome[b].id == WPBIOME_NULLID) break;
	    float *str = bbBiomeIDs.Get(p->biome[b].id, 0.f);
	    *str += float(p->biome[b].blend) * gKernel[kernelOffset];
	  }
	  kernelOffset++;
	}
	
	//Get strongest N biomes
	bbBiomeIDs.Delete(WPBIOME_NULLID);//Delete "null biome"
	Uint8 totalBiomes = (Uint8)bbBiomeIDs.Count();
	//std::cout << ((int)totalBiomes) << ": ";
	for(Uint8 b = 255; b < WPBIOME_BLENDCNT; b++) {
	  if(b >= totalBiomes) {
	    sq->biome[i].id = WPBIOME_NULLID;
	    sq->biome[i].blend = 0;
	  } else {
	    sq->biome[i].id = bbBiomeIDs.Max();
	    sq->biome[i].blend = (Uint8)round( *bbBiomeIDs.Get(sq->biome[i].id) );
	    //std::cout << sq->biome[i].id << "->" << *bbBiomeIDs.Get(sq->biome[i].id) << ", ";
	    bbBiomeIDs.Delete(sq->biome[i].id);
	  }
	}
      }
      //std::cout << "\n";
      progress++;
      Console::Progress(progress);
    }
    bbBiomeIDs.Clear();
    
    for(Uint16 x = minLand.x; x < maxLand.x; x++) {//Y-rel blur
      for(Uint16 y = minLand.y; y < maxLand.y; y++) {
	if(owner->overmap[x][y].biome[0].id == WPBIOME_NULLID) continue;
	
	const Uint16 kMax = y + kQuarter; Uint8 kernelOffset = 0;
	WorldPixel *const sq = &owner->overmap[x][y];
	
	for(Uint16 k = y - kQuarter ; k < kMax; k++) {
	  const WorldPixelTMP *const p = &owner->omTemp[x][k];
	  
	  for(Uint8 b = 0; b < WPBIOME_BLENDCNT; b++) {
	    if(p->biome[b].id == WPBIOME_NULLID) break;
	    float *str = bbBiomeIDs.Get(p->biome[b].id, 0.f);
	    *str += float(p->biome[b].blend) * gKernel[kernelOffset];
	  }
	  kernelOffset++;
	}
	
	//Get strongest N biomes
	Uint8 totalBiomes = (Uint8)bbBiomeIDs.Count();
	for(Uint8 b = 0; b < WPBIOME_BLENDCNT; b++) {
	  if(b >= totalBiomes) {
	    sq->biome[i].id = WPBIOME_NULLID;
	    sq->biome[i].blend = 0;
	  } else {
	    sq->biome[i].id = bbBiomeIDs.Max();
	    sq->biome[i].blend = (Uint8)round( *bbBiomeIDs.Get(sq->biome[i].id) );
	    bbBiomeIDs.Delete(sq->biome[i].id);
	  }
	}
      }
      
      progress++;
      Console::Progress(progress);
    }
    bbBiomeIDs.Clear();
  }  
  
  progress++;
  Console::Progress(progress);
  
  bbBiomeIDs.Clear();
  return true;
}

bool Generator::DerivativeMap() {
  const V2T<Uint16> minLand = owner->omMinLand, maxLand = V2T<Uint16>(owner->omWidth-minLand.x, owner->omHeight-minLand.y);;
  const Uint8 seaThresh = (int)owner->omSeaLevel * 2 / 8;
  
  unsigned long long tHeight = 0; Uint8 minHeight = 255; Uint8 maxHeight = 0;
  const V2T<float> scale = owner->GetOMResolution();
  
  const float kmKernalRange = 3.f;
  V2T<Int16> kRange = V2T<Int16>(ceil(kmKernalRange * scale.x), ceil(kmKernalRange * scale.y));
  V2T<float> kMult =  scale.reciprocal();
  
  for(Uint16 x = 0; x < owner->omWidth; x++) {
    for(Uint16 y = 0; y < owner->omHeight; y++) {
      WorldPixel *const sq = &owner->overmap[x][y];
      if(x < minLand.x || x > maxLand.x || y < minLand.y || y > maxLand.y) {
	sq->dxde = sq->dyde = 0;
	continue;
      }
      
      const Int16 e = sq->elevation;
      tHeight += e; 
      minHeight = std::min(minHeight, sq->elevation);
      maxHeight = std::max(maxHeight, sq->elevation);
      
      if(e < seaThresh) { sq->dxde = sq->dyde = 0; continue; };
      
      Uint8 kernelOffset = 0; float dxde = 0.f, dyde = .0f, dxdeWeight = 0.f, dydeWeight = 0.f;
      /*for(Int16 k = -kQuarter; k < kQuarter; k++) {
	dxde += gKernel[k+kHalf] * float((Int16)owner->overmap[x+k][y  ].elevation - e);
	dxde -= gKernel[k+kHalf] * float((Int16)owner->overmap[x-k][y  ].elevation - e);
	
	dyde += gKernel[k+kHalf] * float((Int16)owner->overmap[x  ][y+k].elevation - e);
	dyde -= gKernel[k+kHalf] * float((Int16)owner->overmap[x  ][y-k].elevation - e);
	
	ddeWeight += gKernel[k+kHalf];
	
	kernelOffset++;
      }*/
      //X and Y separated for differing scales
      for(Int16 k = 0; k < kRange.x; k++) {
	float weight = 1.f - (abs(k) / kRange.x);//1.f / (float)(1+abs(k));
	//weight *= weight;
	dxde += weight * float((Int16)owner->overmap[x+k][y  ].elevation - e);
	dxde -= weight * float((Int16)owner->overmap[x-k][y  ].elevation - e);
	
	dxdeWeight += weight;
      }
      for(Int16 k = 0; k < kRange.y; k++) {
	float weight = 1.f - (abs(k) / kRange.y);//1.f / (float)(1+abs(k));
	//weight *= weight;
	dyde += weight * float((Int16)owner->overmap[x  ][y+k].elevation - e);
	dyde -= weight * float((Int16)owner->overmap[x  ][y-k].elevation - e);
	
	dydeWeight += weight;
      }
      
      sq->dxde = (Int8)round(dxde/(dxdeWeight)*WPDDE_SCALE/-kmKernalRange);
      sq->dyde = (Int8)round(dyde/(dydeWeight)*WPDDE_SCALE/-kmKernalRange);
      /*if(sq->dxde <= -127) {
	std::cout << x << ", " << y << ": " << ((int)e) << "\n";
      }*/
      
    }
  }
  tHeight /= (unsigned long long) owner->omWidth * owner->omHeight;
  owner->omAvgElevation = (Uint8)(tHeight );
  
  const Int8 upperSec = abs( (maxHeight - ((int)tHeight)) / WPELDE_SCALE);
  const Int8 lowerSec = -abs( ((int)(tHeight) - minHeight) / WPELDE_SCALE);
  const Uint8 mid = (Uint8)tHeight;
  
  //Catagorize height percent/deviation
  for(Uint16 x = minLand.x; x < maxLand.x; x++) {
    for(Uint16 y = minLand.y; y < maxLand.y; y++) {
      WorldPixel *const sq = &owner->overmap[x][y];
      const Uint8 e = sq->elevation;
      const Uint8 dist = (e > mid) ? (e - mid) : (mid - e);
      sq->elevationDev = dist / ((e > mid) ? upperSec : lowerSec);
    }
  }
  
  return true;
}

static Uint8 genOMSeaLevel; static WorldPixel **om = NULL;
static Uint16 omMaxX, omMaxY;
void oceanFF(Uint16 x, Uint16 y, const Uint8 &f) {
  if(x >= omMaxX || y >= omMaxY) return;
  
  WorldPixel *const sq = &om[x][y];
  if(sq->waterState != WPWS_NA) return;
  
  if(sq->moisture != 255) sq->waterState = WPWS_LAND;
  else {
    sq->waterState = f;
    oceanFF(x+1, y  , f);
    oceanFF(x-1, y  , f);
    oceanFF(x  , y+1, f);
    oceanFF(x  , y-1, f);
  }
}

void Generator::MarkBasicFlags() {
  Uint16 height = omMaxY = owner->omHeight, width = omMaxX = owner->omWidth;
  genOMSeaLevel = owner->omSeaLevel;
  om = owner->overmap;
  //X-prefill from edge
  for(Uint16 x = 0; x < width; x++) {
    bool pfill = true, mfill = true;
    for(Uint16 y = 0; y < height/2; y++) {
      WorldPixel *const ps = &owner->overmap[x][y];
      WorldPixel *const ms = &owner->overmap[x][height-y-1];
      if(pfill && ps->moisture != 255) {
	ps->waterState = WPWS_LAND;
	pfill = false;
      } else if(!pfill && ps->moisture != 255) {
	ps->waterState = WPWS_LAND;
      } else if(pfill) {
	ps->waterState = WPWS_OCEAN;
      }
      
      if(mfill && ms->moisture != 255) {
	ms->waterState = WPWS_LAND;
	mfill = false;
      } else if(!mfill && ms->moisture != 255) {
	ms->waterState = WPWS_LAND;
      } else if(mfill) {
	ms->waterState = WPWS_OCEAN;
      }
    }
  }
  
  //Y-prefill from edge
  for(Uint16 y = 0; y < height; y++) {
    bool pfill = true, mfill = true;
    for(Uint16 x = 0; x < width/2; x++) {
      WorldPixel *const ps = &owner->overmap[x][y];
      WorldPixel *const ms = &owner->overmap[width-x-1][y];
      if(pfill && ps->moisture != 255) {
	ps->waterState = WPWS_LAND;
	pfill = false;
      } else if(!pfill && ps->moisture != 255) {
	ps->waterState = WPWS_LAND;
      } else if(pfill) {
	ps->waterState = WPWS_OCEAN;
      }
      
      if(mfill && ms->moisture != 255) {
	ms->waterState = WPWS_LAND;
	mfill = false;
      } else if(!mfill && ms->moisture != 255) {
	ms->waterState = WPWS_LAND;
      } else if(mfill) {
	ms->waterState = WPWS_OCEAN;
      }
    }
  }
  
  //Standard recursion to close gaps - WARNING: May stackoverflow and large OM sizes... FIXME
  for(Uint16 x = 0; x < width; x++) {
    for(Uint16 y = 0; y < height; y++) {
      WorldPixel *const sq = &owner->overmap[x][y];
      if(sq->waterState == WPWS_OCEAN) {
	oceanFF(x+1, y  , WPWS_OCEAN);
	oceanFF(x-1, y  , WPWS_OCEAN);
	oceanFF(x  , y+1, WPWS_OCEAN);
	oceanFF(x  , y-1, WPWS_OCEAN);
      }
    }
  }
  
  //All oceans were filled- all else are lakes
  for(Uint16 x = 0; x < width; x++) {
    for(Uint16 y = 0; y < height; y++) {
      WorldPixel *const sq = &owner->overmap[x][y];
      if(sq->moisture == 255 && sq->waterState == WPWS_NA) {
	sq->waterState = WPWS_LAKE;
      }
    }
  }
}

bool Generator::ClassifyBiomes() {
  const V2T<Uint16> minLand = owner->omMinLand * 0.0f, maxLand = V2T<Uint16>(owner->omWidth-minLand.x, owner->omHeight-minLand.y);
  
  Console::Progress("[%p] Classifying biomes...", maxLand.x - minLand.x);
  for(Uint16 x = minLand.x; x < maxLand.x; x++) {
    for(Uint16 y = minLand.y; y < maxLand.y; y++) {
      WorldPixel *const sq = &owner->overmap[x][y];
      Biome::Classify(sq);//Alters square as needed
    }
    Console::Progress(x);
  }
  Console::Progress(maxLand.x - minLand.x);
  
  return true;
}

/*
bool Generator::Generate(const WorldGenData& newData) {
  owner->omOctaves = 15;
  
  data = newData;
  
  SetupContinent();
  SetupMagic();
  SetupVolcanos();
  
  DebugImage img(data.width, data.height);
  
  std::cout << "Generating base elevation and temperature data...\n";
  //Generate Stage I data
  worldMap = new WorldPixel*[data.width];
  for(int x = 0; x < data.width; x++) {
    worldMap[x] = new WorldPixel[data.height];
    for(int y = 0; y < data.height; y++) {
      WorldPixel *p = &worldMap[x][y];
      
      float xCoord = float(x)/data.scale, yCoord = float(y)/data.scale;
      
      int elevation = floor(255.0 * continentComplete->GetValue(xCoord, yCoord, 0.0));
      int lTemp = getBaseTemp(x, y, elevation);
      
      int magic = 0;//floor(255.0 * magicCurve->GetValue(xCoord, yCoord, 0.0));
      int volcanic = 0;//floor(255.0 * volcanicCurve->GetValue(xCoord, yCoord, 0.0));
      int tempurature = getBaseTemp(elevation);
      
      elevation = XMath::Clamp(elevation, 0, 255);
      magic = XMath::Clamp(magic, 0, 255);
      volcanic = XMath::Clamp(tempurature, 0, 255);
      
      p->elevation = elevation;
      p->lTemp = lTemp;
      //p->moisture
      //if(elevation < WATER_LEVEL) elevation = 0;
    }
  }
  
  //Simulate wind to generate base moisture and alter temperature
  std::cout << "simulating winds...\n";
  const Uint8 windVCount = 4;
  V2T<Int16> windVectors[windVCount];
  windVectors[0] = V2T<Int16>{ 1, 1};
  windVectors[1] = V2T<Int16>{-1,-1};
  windVectors[2] = V2T<Int16>{-1, 1};
  windVectors[3] = V2T<Int16>{ 1,-1};
  for(Uint8 v = 0; v < windVCount; v++) {
    V2T<Int16> vector = windVectors[v];
    //for()
  }
  
  std::cout << "creating world map...\n";
  for(int x = 0; x < data.width; x++) {
    for(int y = 0; y < data.height; y++) {
      WorldPixel *p = &worldMap[x][y];
      
      
      int drawElev = p->elevation;
      if(drawElev < WATER_LEVEL) drawElev = 0;
      int color = img.Color((p->lTemp / 10) + 60, drawElev, 0);
      img.Pixel(x, y, color);
    }
  }
  std::stringstream ss; ss << "height" << owner->seed << ".png";
  img.Write(ss.str());
  return true;
}*/

/*
int Generator::getBaseTemp(int x, int y, int elevation) {
  //x = round(((float)x / data.width) * data.width);
  //y = round(((float)y / data.height) * data.height);
  const float waterTempDistance = 12;
  float shoreModifier = 1.0f / ceil((float)std::max(elevation - WATER_LEVEL, 0) / waterTempDistance + 4.0f);//More temperamental close to water
  
  const int hCoordDT = sqrt(pow2(data.hTempX - x) + pow2(data.hTempY - y));
  const int lCoordDT = sqrt(pow2(data.lTempX - x) + pow2(data.lTempY - y));
  const float coordTempRatio = lCoordDT / (float)(hCoordDT + lCoordDT);
  float coordTemp = lerp(data.lTemp, data.hTemp, coordTempRatio);
  //std::cout << coordTemp <<":" << hCoordDT << "," << lCoordDT << " (" << x << ", " << y << ") \n";
  
  const int tempElevMin = 110;
  float elevModifier = std::max(0.0f, (float)elevation - tempElevMin) / (256.0f - tempElevMin) * 2.0f;
  
  float temp = 127.0f;//default tempurature
  temp = temp * shoreModifier;
  
  return round(lerp(coordTemp, (float)data.aTemp, shoreModifier));
}*/

void Generator::SetupContinent() {
  double continentFrequency = 0.1;
  
  //Continent Base
  continentBase = new noise::module::Perlin();
  continentBase->SetOctaveCount (owner->omOctaves);
  continentBase->SetFrequency (0.1);
  continentBase->SetPersistence (0.612);
  continentBase->SetLacunarity(1.772);
  continentBase->SetSeed(owner->seed ^ XOR_CONTINENT);
  
  continentScale = new noise::module::ScaledBase();
  continentScale->SetSourceModule(0, *continentBase);
  const V2T<float> genScale = owner->GetGenScale();
  continentScale->SetCenter(float(owner->omWidth) / genScale.x / 2.f, float(owner->omHeight) / genScale.y / 2.f);
  continentScale->SetBases(-1.0, 0.6);
  continentScale->SetMods(0.0, 1.0);
  continentScale->SetRange(0.0, 1.0);
  
  //Noise
  continentNoiseRaw = new noise::module::Perlin();
  continentNoiseRaw->SetOctaveCount (owner->omOctaves);
  continentNoiseRaw->SetFrequency (1.4);
  continentNoiseRaw->SetPersistence (0.612);
  continentNoiseRaw->SetLacunarity(1.272);
  continentNoiseRaw->SetSeed(owner->seed ^ (XOR_CONTINENT - 1));
  
  continentNoiseScaled = new noise::module::ScaleBias();
  continentNoiseScaled->SetSourceModule (0, *continentNoiseRaw);
  continentNoiseScaled->SetScale (0.02);
  continentNoiseScaled->SetBias (0.015);
  
  //Carver
  continentCarver_raw = new noise::module::RidgedMulti();
  continentCarver_raw->SetOctaveCount (owner->omOctaves);
  continentCarver_raw->SetFrequency(continentFrequency * 2.2);
  //continentCarver_raw->SetPersistence(0.6);
  continentCarver_raw->SetSeed(owner->seed ^ (XOR_CONTINENT + 0xFF));
  
  continentCarver = new noise::module::ScaleBias();
  continentCarver->SetSourceModule (0, *continentCarver_raw);
  continentCarver->SetScale (-0.0);
  continentCarver->SetBias (0.8);
  
  continentCarved = new noise::module::Min();
  continentCarved->SetSourceModule (0, *continentScale);
  continentCarved->SetSourceModule (1, *continentCarver);
  
  //Apply noise
  continentPostNoise = new noise::module::Add();
  continentPostNoise->SetSourceModule (0, *continentCarved);
  continentPostNoise->SetSourceModule (1, *continentNoiseScaled);
  
  continentCurve = new noise::module::Curve();
  continentCurve->SetSourceModule(0, *continentPostNoise);
  continentCurve->AddControlPoint (0.0, 0.0);
  continentCurve->AddControlPoint (0.2, 0.2);
  continentCurve->AddControlPoint (0.4, 0.38);
  continentCurve->AddControlPoint (0.6, 0.52);
  continentCurve->AddControlPoint (0.85, 0.72);
  continentCurve->AddControlPoint (1.0, 1.0);
  
  continentComplete = new noise::module::Clamp();
  
  continentComplete->SetSourceModule (0, *continentCurve);
  continentComplete->SetBounds (-1.0, 1.0);
}

void Generator::SetupMagic() {
  //Magic
  magicBase = new noise::module::Billow();
  magicBase->SetSeed(owner->seed ^ XOR_MAGIC);
  magicBase->SetFrequency (0.1);
  magicBase->SetPersistence (0.6);
  magicBase->SetOctaveCount (owner->omOctaves);
  
  magicCurve = new noise::module::Curve();
  magicCurve->SetSourceModule(0, *magicBase);
  magicCurve->AddControlPoint(-1.0, 1);
  magicCurve->AddControlPoint(-0.8, 0.2);
  magicCurve->AddControlPoint( 0.0, 0.7);
  magicCurve->AddControlPoint( 0.6, 1.0);
  magicCurve->AddControlPoint( 0.88, 1.0);
  magicCurve->AddControlPoint( 1.0, 0.0);
}

void Generator::SetupVolcanos() {
  
  volcanicBase = new noise::module::RidgedMulti();
  volcanicBase->SetOctaveCount (owner->omOctaves);
  volcanicBase->SetFrequency (0.05);
  //volcanicBase->SetPersistence (0.6);
  volcanicBase->SetLacunarity(2.25);
  volcanicBase->SetSeed(owner->seed ^ XOR_VOLCANIC);
  
  volcanicCurve = new noise::module::Curve();
  volcanicCurve->SetSourceModule(0, *volcanicBase);
  volcanicCurve->AddControlPoint(-1.0, -1.0);
  volcanicCurve->AddControlPoint( 0.0, -1.0);
  volcanicCurve->AddControlPoint( 0.75, -1.0);
  volcanicCurve->AddControlPoint( 1.0, 1.0);
}