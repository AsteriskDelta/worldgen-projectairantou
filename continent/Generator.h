#pragma once
#include "include.h"

class Generator {
friend class Continent;
public:
  Generator(Continent *const newOwner);
  ~Generator();
  
  bool Init();
  bool GenerateBasics();
  bool SimulateWind(Uint16 iterMax);
  bool BlurMoisture(Uint16 iters);
  bool BlurBiomes(Uint16 iters);
  bool DerivativeMap();
  
  void MarkBasicFlags();//Ocean/Lake etc.
  
  bool ClassifyBiomes();
  
private:
  Continent* owner;
  void SetupContinent();
  void SetupMagic();
  void SetupVolcanos();
  
//#ifdef WORLDGEN_CPP
  noise::module::Perlin* continentBase;
  noise::module::Curve * continentCurve;
  noise::module::ScaledBase* continentScale;
  noise::module::RidgedMulti* continentCarver_raw;
  noise::module::ScaleBias* continentCarver;
  noise::module::Min* continentCarved;
  noise::module::Clamp* continentComplete;
  
  noise::module::Add* continentPostNoise;
  noise::module::Perlin* continentNoiseRaw;
  noise::module::ScaleBias* continentNoiseScaled;
  
  noise::module::Billow* magicBase;
  noise::module::Curve* magicCurve;
  
  noise::module::RidgedMulti* volcanicBase;
  noise::module::Curve* volcanicCurve;
//#endif
};