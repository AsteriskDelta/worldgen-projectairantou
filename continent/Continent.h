#pragma once
#include "shared/Shared.h"

#define WPT_C(x) (float(x) / 8.f)
#define C_WPT(x) ((Int16)round(x * 8.f))

#define CONT_TEMPSAMP_MAX 64

#define WPWIND_VMULT 7.f
#define WPWIND_DDIV  0.5f
#define WPBIOME_BLENDCNT 2

#define WPELDE_SCALE 6

#define WPDDE_SCALE 6.f

#define WPWS_LAND  0
#define WPWS_OCEAN 1
#define WPWS_LAKE  2
#define WPWS_NA    3

#define WPBIOME_BSTRM 31.f
#define WPBIOME_NULLID 0x7FF

#define CONT_ABSMAX_OM 32768

#define CONT_OV_BASE 256.f

struct WPWind {
  Int8 x : 4;
  Int8 y : 4;
};

struct WPBiome {
  Uint16 id : 11;
  Uint8 blend : 5;
};

union WPFlags {
  struct {
    Uint8 waterState : 2;//Land, Ocean, Lake
    bool processed : 1;
    Uint8 padding : 5;
  };
  Uint8 flags;
};

struct WorldPixel {
  Uint8 elevation;
  Uint8 moisture;
  Uint8 aura;
  struct {
    Int16 temp	 : 12;//base, /= 8.f for C
    //4 bits free
    Int8 elevationDev : 4;
  };
  union {//Interlaced data
    WPWind wind;//x+0 % 8, y+0 % 8
  };
  struct {
    Int8 dxde : 8;
    Int8 dyde : 8;
  };
  WPBiome biome[WPBIOME_BLENDCNT];
  union {//WARNING:Alias WPFlags!!!
    struct {
      Uint8 waterState : 2;//Land, Ocean, Lake
      bool processed : 1;
      Uint8 ffID : 2;//flip-flop state
      Uint8 padding : 5;
    };
    Uint8 flags;
  };
  //3 Bytes remaining
  Uint8 bPadding[3];
};

struct WorldVexel {
  WPWind wind;
  Int16 pMoisture;
};

struct WorldPixelTMP {//Processing data
  union {//4B
    WPBiome biome[WPBIOME_BLENDCNT];
  };
  Uint8 moisture;
  Uint8 padding[3];
};

class Generator;
class Continent {
friend class Generator;
public:
  Continent();
  ~Continent();
  
  void SetSize(Uint16 nw, Uint16 nh);
  
  void SetOMSize(Uint16 nw, Uint16 nh);
  V2T<float> GetOMResolution();//px per km
  V2T<float> GetOMScale();//km per px
  V2T<float> GetGenScale();//Maps to generator units
  V2T<float> GetOMMult();
  
  
  Int16 SetBaseTempWind(V2T<Uint16> position);
  
  bool GenerateOvermap();
  bool DebugOvermap(std::string path);
  
  //Access in KM
  WorldPixel GetOMPixel(float x, float y);
  WorldVexel GetOMVexel(float x, float y);
  
  bool SaveBase(std::string path);
  bool LoadBase(std::string path);
  
  void ClearTempFlags();
  
  /* Base data */
  std::string name;
  Uint64 seed;
  V2T<Uint16> center;
  
  Uint16 width, height;//km
  Uint16 omWidth, omHeight;//px
  Uint16 ovWidth, ovHeight;
  V2T<Uint16> omMinLand, omMaxLand;
  float omVerticalScale;//km per px
  WorldPixel **overmap;
  WorldVexel **vexelmap;
  WorldPixelTMP **omTemp;
  
  Uint8 omAvgElevation;
  
  /* Overmap Generation Data */
  Uint8 omSeaLevel;
  int omOctaves;
  //Temp samples at (-1,-1) (1,-1) (1,1) (-1, 1) and (0,0) - Centigrade
  Uint8 tempAvgCount;
  float tempAvgSamples[CONT_TEMPSAMP_MAX];
private:
  Generator *generator;
  
  void allocateOvermap();
  void deleteOvermap();
};