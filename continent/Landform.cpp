#include "Landform.h"
#include "landforms/Crater.h"
#include "landforms/River.h"

Landform* Landform::landforms[MAX_LANDFORMS];
Uint16 Landform::landformCount = 0;

void Landform::LoadAll() {
  landformCount = 0;
  landforms[landformCount] = new Crater(); landformCount++;
  //Rift
  //Volcano
  //Mountain Ranges
  landforms[landformCount] = new River(); landformCount++;
  //Straits
  
  for(int i = 0; i < landformCount; i++) {
    Landform *const l = landforms[i];
    for(int j = 0; j < landformCount; j++) {
      if(i != j && l->GetXOR() == landforms[j]->GetXOR()) {
	std::stringstream ss;
	ss << "Landforms " << i << " and " << j << " have the same XOR ID (" << std::hex << l->GetXOR() << std::dec << ")";
	Console::Error(ss.str());
      }
    }
  }
}

void Landform::ActiveContinent(Continent *const n) {
  for(Uint16 i = 0; i < landformCount; i++) {
    landforms[i]->SetOwner(n);
  }
}

void Landform::AllPostBasic() {
  for(Uint16 i = 0; i < landformCount; i++) {
    landforms[i]->tPostBasic();
  }
}

void Landform::AllPostWind() {
  for(Uint16 i = 0; i < landformCount; i++) {
    landforms[i]->tPostWind();
  }
}

void Landform::AllDebugDraw(DebugImage *img) {
  for(Uint16 i = 0; i < landformCount; i++) {
    landforms[i]->tDebugDraw(img);
  }
}

void Landform::SaveAll(std::ofstream *const fst) {
  for(Uint16 i = 0; i < landformCount; i++) {
    landforms[i]->Save(fst);
  }
}

void Landform::LoadAll(std::ifstream *const fst) {
  for(Uint16 i = 0; i < landformCount; i++) {
    landforms[i]->Load(fst);
  }
}

void Landform::SetOwner(Continent *const n) {
  owner = n;
}

Uint32 Landform::GetRandom() {
  randXor = (8253729 * randXor + 2396403); 
  randCnt++;
  return randXor;// % 32767;
}

Landform::Landform() {};
Landform::~Landform() {};

//Universals
void Landform::GenerateInstances() {};
void Landform::DeleteInstances() {};

void Landform::Save(std::ofstream *const fst) {
  _unused(fst);
}

void Landform::Load(std::ifstream *const fst) {
  _unused(fst);
}

Uint32 Landform::GetXOR() { return 0x00000000; };

//Generation triggers
bool Landform::tPostBasic() { return false; };
bool Landform::tPostWind() { return false; };
void Landform::tDebugDraw(DebugImage *img) { _unused(img); return; };