#include "Crater.h"
#include "../Continent.h"
#include <noise/noise.h>
#include <fstream>

Crater::Crater() {
  owner = NULL;
  instanceCount = 0;
  instances = NULL;
  pNoiseBase = NULL;
}
Crater::~Crater() {
  DeleteInstances();
}

void Crater::SetOwner(Continent *const n) {
  owner = n; randCnt = 0;
  randXor = owner->seed ^ LFCRATER_XOR;
  if(pNoiseBase != NULL) delete pNoiseBase;
  pNoiseBase = new noise::module::RidgedMulti();
  pNoiseBase->SetOctaveCount (owner->omOctaves);
  pNoiseBase->SetFrequency (0.08);
  //volcanicBase->SetPersistence (0.6);
  pNoiseBase->SetLacunarity(1.08);
  pNoiseBase->SetSeed(randXor);
  GetRandom();
}

void Crater::GenerateInstances() {
  instanceCount = this->GetRandom() % 6 + 3;
  instances = new LFCraterInstance[instanceCount];
  std::stringstream ss; ss << "World codes for " << instanceCount << " crater(s).";
  Console::Print(ss.str());
  
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFCraterInstance *const crater = &instances[i];
    //Get a crater strength, biased towards average
    crater->strength = (getRRange(0, 100));
    
    //Locate position with center on land
    V2T<Uint16> pos;
    while(true) {
      pos = V2T<Uint16>(round((float)getRRange(1, CONT_ABSMAX_OM) / (float)CONT_ABSMAX_OM * owner->omWidth - 1), round((float)getRRange(1, CONT_ABSMAX_OM) / (float)CONT_ABSMAX_OM * owner->omHeight - 1));
      if(owner->overmap[pos.x][pos.y].elevation > owner->omSeaLevel) break;
    }
    crater->pos = pos;
  }
}

void Crater::DeleteInstances() {
  if(instances != NULL) delete[] instances;
  instanceCount = 0;
}

void Crater::Save(std::ofstream *const fst) {
  char tmp[1] = {'I'};
  fst->write(tmp, 1);
  fst->write((char*)&instanceCount, sizeof(Uint16));
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFCraterInstance *const crater = &instances[i];
    fst->write((char*)crater, sizeof(LFCraterInstance));
  }
}

void Crater::Load(std::ifstream *const fst) {
  char tmp[1];
  fst->read(tmp, 1);
  fst->read((char*)&instanceCount, sizeof(Uint16));
  
  instances = new LFCraterInstance[instanceCount];
  
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFCraterInstance *const crater = &instances[i];
    fst->read((char*)crater, sizeof(LFCraterInstance));
  }
}

//Generation triggers
bool Crater::tPostBasic() {
  this->GenerateInstances();
  
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFCraterInstance *const crater = &instances[i];
    V2T<float> scale = owner->GetOMResolution();
    float radius = (crater->strength / 3.f);// / std::max(scale.x, scale.y);// Up to 500 M
    float depth = (crater->strength / 300.f) / owner->omVerticalScale;
    
    const Uint16 xMin = std::max(1, (int)round(crater->pos.x - radius*scale.x));
    const Uint16 xMax = std::min(owner->omWidth, (Uint16)round(crater->pos.x + radius*scale.x));
    const Uint16 yMin = std::max(1, (int)round(crater->pos.y - radius*scale.y));
    const Uint16 yMax = std::min(owner->omHeight, (Uint16)round(crater->pos.y + radius*scale.y));
    
    const float xDist = (xMax - xMin), yDist = (yMax - yMin);
    const float xMid = xDist/2.f + xMin, yMid = yDist/2.f + yMin;
    
    const float peakSize = 1.2f, rimSize = 0.4f, bowlSize = 1.f;
    for(Uint16 x = xMin; x < xMax; x++) {
      for(Uint16 y = yMin; y < yMax; y++) {
	const float xRel = abs((float)x - xMid) / xDist;
	const float yRel = abs((float)y - yMid) / yDist;
	const float dist = sqrt(xRel*xRel + yRel*yRel);
	
	const float peakStrength = std::max((float)(-pow(dist*7.f, 2)+1), 0.f);//Active 0->1/8
	const float bowlStrength = std::min((float)pow(dist*2.f, 1.6f) - 1.f, 0.f);//Actuve 0->1/2
	const float rimInnerStr = std::max((float)(-pow((dist-0.3f)*26.f, 2)+1), 0.f);
	const float rimOuterStr = std::max((float)(-pow((dist-0.45f)*32.f, 2)+1) * 0.25f, 0.f);
	
	const float deltaH = clamp((rimInnerStr + rimOuterStr) * rimSize + bowlStrength * bowlSize + peakStrength * peakSize, -1.f, 1.f) * (pNoiseBase->GetValue(x, y, 0.0) * 0.5f + 1.f);
	
	WorldPixel *const sp = &(owner->overmap[x][y]);
	float newElevation = clamp((float)round(float(sp->elevation) + deltaH*depth), 0.f, 255.f);
	sp->elevation = (Uint8)newElevation;
	if(sp->elevation < owner->omSeaLevel) sp->moisture = 255;
      }
    }
  }
  
  return true;
}

bool Crater::tPostWind() {
  
  return true;
}

void Crater::tDebugDraw(DebugImage *img) {
  _unused(img);
}

Uint32 Crater::GetXOR() {
  return LFCRATER_XOR;
}