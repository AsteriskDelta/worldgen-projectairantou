#include "River.h"
#include "../Continent.h"
#include <noise/noise.h>
#include "lib/DebugImage.h"
#include "Random.h"

River::River() {
  owner = NULL;
  instanceCount = 0;
  instances = NULL;
  pNoiseBase = NULL;
}
River::~River() {
  DeleteInstances();
}

void River::SetOwner(Continent *const n) {
  owner = n; randCnt = 0;
  randXor = owner->seed ^ LFRIVER_XOR;
  /*if(pNoiseBase != NULL) delete pNoiseBase;
  pNoiseBase = new noise::module::RidgedMulti();
  pNoiseBase->SetOctaveCount (owner->omOctaves);
  pNoiseBase->SetFrequency (2);
  pNoiseBase->SetLacunarity(1.15);
  pNoiseBase->SetSeed(randXor);*/
  GetRandom();
}

static Uint8 lakeMaxElevation = 0, useFFID = 0; static WorldPixel **om = NULL;
static Uint16 omMaxX, omMaxY; static bool omDoFill = false, omDoTrace = false;;
static Uint64 omFillCount = 0; static V2T<Uint16> failCoord;
bool lakeFF(Uint16 x, Uint16 y, Int16 depth) {
  if(x >= omMaxX || y >= omMaxY) return true;
  
  WorldPixel *const sq = &om[x][y];
  if(sq->ffID == useFFID) return true;
  sq->ffID = useFFID;
  bool result = true;
  
  if(sq->elevation < lakeMaxElevation/* && sq->waterState == WPWS_LAND*/) {
    omFillCount++; depth--;
    if(depth == 0) return false;
    else if (omDoFill) {
      sq->moisture = 255;
      sq->waterState = WPWS_LAKE;
    }
    result &= lakeFF(x+1, y  , depth);
    result &= lakeFF(x-1, y  , depth);
    result &= lakeFF(x  , y+1, depth);
    result &= lakeFF(x  , y-1, depth);
    if(!result && omDoTrace) {
      if(sq->waterState == WPWS_LAKE) {
	omDoTrace = false;
	failCoord = V2T<Uint16>(x, y);
      } else {
	failCoord = V2T<Uint16>(x, y);
      }
    }
  }
  
  return result;
}

void River::GenerateInstances() {
  instanceCount = this->GetRandom() % 80 + 56;
  instances = new LFRiverInstance[instanceCount];
  const Uint8 minElevation = owner->omAvgElevation * .8f + 210.f * .2f;
  const Uint8 minMoisture = 2;
  const V2T<float> scale = owner->GetOMScale();
  const float maxRLen = (V2T<float>(owner->omWidth, owner->omHeight) * scale).magnitude()/2.f;
  const float minRunningDistance = 12.f;
  const Uint16 lakeDescentMax = 1800;
  
  std::stringstream ss; ss << "World codes for " << instanceCount << " major rivers and " << 0 << " straits, elvation +" << ((int)minElevation);
  Console::Print(ss.str());
  int cdCount = 0;
  V2T<Uint16> pos; bool useSuggestedPos = false;
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFRiverInstance *const river = &instances[i];
    river->hasFCoord = river->hasLake = false;
    int startingElevation, endingElevation;
    //Locate position with center on land
    if(useSuggestedPos && owner->overmap[pos.x][pos.y].dxde == 0 && owner->overmap[pos.x][pos.y].dyde == 0) useSuggestedPos = false;
    
    Random::Seed(GetRandom() * (i*53655 + 1));
    while(!useSuggestedPos) {
      pos = V2T<Uint16>(Random::Range(1.f, (float)owner->omWidth-1), Random::Range(1.f, (float)owner->omHeight-1));
      //pos = V2T<Uint16>(Random::Gaussian(owner->omWidth / 2, owner->omWidth / 2), Random::Gaussian(owner->omWidth / 2, owner->omWidth / 2));
      WorldPixel *const sq = &owner->overmap[pos.x][pos.y];
      //std::cout << "Attempting " << pos.ToString() << "\n";
      if(/*sq->elevation> minElevation && sq->moisture > minMoisture&& */(sq->dxde != 0 || sq->dyde != 0)) break;
    }
    river->nodes[0].pos = pos;
    river->nodes[0].flow = owner->overmap[pos.x][pos.y].moisture;
    river->nodeCount = 1;
    
    V2T<float> pt = V2T<float>(pos.x, pos.y);
    V2T<float> dpt = V2T<float>(owner->overmap[pos.x][pos.y].dxde, owner->overmap[pos.x][pos.y].dyde) * scale / WPDDE_SCALE;
    float dptLength = dpt.magnitude();
    Uint8 prevElevation = owner->overmap[pos.x][pos.y].elevation;
    startingElevation = endingElevation = prevElevation;
    //Uint8 nCount = 1;
    float runningElevation = prevElevation+1;
    float runningDistance = 0.f, dbgDistance = 0.f;
    while(river->nodeCount < 256) {
      WorldPixel *const sq = &owner->overmap[(Uint16)roundf(pt.x)][(Uint16)roundf(pt.y)];
      V2T<float> delta = V2T<float>(float(sq->dxde)/WPDDE_SCALE, float(sq->dyde)/WPDDE_SCALE) * scale;
      dptLength += delta.magnitude();
      
      float ptStr = scale.magnitude();//(delta.magnitude() / dptLength);
      float dtStr = pow(ptStr / dptLength, 0.03f);
      float rtStr = scale.magnitude();
      if(delta.magnitude() < 0.08f) dtStr = 0.f;
      
      dpt = delta * dtStr + dpt * (1-dtStr);
      if(dpt.magnitude() < 0.01f || dbgDistance > maxRLen) { runningDistance = 0; break; }//Break and force rejection
      
      //std::cout << pt.x << ", " << pt.y << " @ " << (int)sq->elevation << " < " << runningElevation << "\n";
      if(sq->elevation < owner->omSeaLevel-1) break; //Success
      else if(sq->elevation < prevElevation) {
	prevElevation = sq->elevation;
	LFRiverNode *const rNode = &river->nodes[river->nodeCount];
	rNode->pos =  V2T<Uint16>(roundf(pt.x), roundf(pt.y));
	rNode->flow = sq->moisture;
	
	LFRiverNode *const prevNode = &river->nodes[river->nodeCount-1];
	runningDistance += (V2T<float>(rNode->pos.x, rNode->pos.y)*scale).distanceTo(V2T<float>(prevNode->pos.x, prevNode->pos.y)*scale);
	river->nodeCount++;
	endingElevation = sq->elevation;
	//std::cout << "addnode\n";
      } else if(sq->elevation > runningElevation) {//End case
	//std::cout << "break\n";
	break;
      }
      
      runningElevation = runningElevation * (1.f-rtStr) + sq->elevation * rtStr;
      pt += dpt.normalized();
      dbgDistance += dpt.magnitude();
    }
    
    bool failed = false;
    LFRiverNode *const lastNode = &river->nodes[river->nodeCount-1];
    const Uint8 lakeThresh = 1;
    if(endingElevation > owner->omSeaLevel && runningDistance >= minRunningDistance) {//Attempt to build a lake, or fail
     omMaxX = owner->omMaxLand.x;
     omMaxY = owner->omMaxLand.y;
     om = owner->overmap;
     bool result = true;
     lakeMaxElevation = endingElevation; useFFID = 1;
     omDoFill = false;
     owner->ClearTempFlags();
     
     while(lakeMaxElevation < 255) {
       omFillCount = 0;
       result = lakeFF(lastNode->pos.x, lastNode->pos.y, lakeDescentMax);
       
       useFFID = (useFFID+1)%3 + 1;
       if(!result) {lakeMaxElevation--; break; }//Return last good max elev
       else lakeMaxElevation++;
     }
     
     if(lakeMaxElevation <= endingElevation + lakeThresh) failed = true;
    }
    
    //Retry test
    if(runningDistance < minRunningDistance || failed) {
      //std::cout << "rejected canidate, retrying...\n";
      useSuggestedPos = false;
      cdCount++;
      i--;
    } else if(cdCount > 80000) {//Emergency exit
      Console::Warning("Unable to generate required number of rivers, return early...");
      if(i > 0) instanceCount = i-1;
      else instanceCount = 0;
      return;
    } else {
      //Commit moisture changes
      if(endingElevation > owner->omSeaLevel) {//Spawn lake if needed
	omDoFill = true;
	lakeFF(lastNode->pos.x, lastNode->pos.y, lakeDescentMax);
	river->hasLake = true;
	
	//Find leading river
	omDoFill = false; omDoTrace = true;
	lakeFF(lastNode->pos.x, lastNode->pos.y, lakeDescentMax);
	useSuggestedPos = true;
	pos = failCoord;
	river->hasFCoord = true;
	river->fCoord = failCoord;
	
	ss.str(""); ss << "Creating lake at " << lastNode->pos.ToString() << " @ " << endingElevation << " to " << ((int)lakeMaxElevation) << ", " << omFillCount << " tiles included\n";
	cdCount = 0;
	Console::Print(ss.str());
      }
      
      ss.str(""); ss << "Major river starting at " << river->nodes[0].pos.ToString() << " @ " << startingElevation <<  " and running for " << runningDistance << "km to e=" << endingElevation << " generated with canidate #" << cdCount;
      cdCount = 0;
      Console::Print(ss.str());
    }
  }
}

void River::DeleteInstances() {
  if(instances != NULL) delete[] instances;
  instanceCount = 0;
}

void River::Save(std::ofstream *const fst) {
  char tmp[1] = {'I'};
  fst->write(tmp, 1);
  fst->write((char*)&instanceCount, sizeof(Uint16));
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFRiverInstance *const river = &instances[i];
    fst->write((char*)river, sizeof(LFRiverInstance));
  }
}

void River::Load(std::ifstream *const fst) {
  char tmp[1];
  fst->read(tmp, 1);
  fst->read((char*)&instanceCount, sizeof(Uint16));
  
  instances = new LFRiverInstance[instanceCount];
  
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFRiverInstance *const river = &instances[i];
    fst->read((char*)river, sizeof(LFRiverInstance));
  }
}

//Generation triggers
bool River::tPostBasic() {
  
  
  return true;
}

bool River::tPostWind() {
  this->GenerateInstances();
  
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFRiverInstance *const river = &instances[i];
    _unused(river);
  }
  
  return true;
}

void River::tDebugDraw(DebugImage *img) {
  for(Uint16 i = 0; i < instanceCount; i++) {
    LFRiverInstance *const river = &instances[i];
    int c = img->Color(0, 255, 255);
    img->Point(river->nodes[0].pos.x, river->nodes[0].pos.y, c);
    for(Uint16 n = 1; n < river->nodeCount; n++) {
      img->Line(river->nodes[n-1].pos.x, river->nodes[n-1].pos.y, river->nodes[n].pos.x, river->nodes[n].pos.y,c); 
    }
    if(river->hasFCoord) {
      c = img->Color(255, 64, 255);
      img->Point(river->fCoord.x, river->fCoord.y, c);
    }
  }
}

Uint32 River::GetXOR() {
  return LFRIVER_XOR;
}