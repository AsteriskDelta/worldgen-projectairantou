#pragma once
#include "../Landform.h"

#define LFCRATER_XOR 0x290CE1D6

struct LFCraterInstance {
  V2T<Uint16> pos;
  float strength;
};

class Crater : public Landform {
public:
  Crater();
  ~Crater();
  
  void SetOwner(Continent *const n);
  
  Uint16 GetMaxInstances();
  void GenerateInstances();
  void DeleteInstances();
  
  void Save(std::ofstream *const fst);
  void Load(std::ifstream *const fst);
  
  //Generation triggers
  bool tPostBasic();
  bool tPostWind();
  void tDebugDraw(DebugImage *img);
  
  Uint32 GetXOR();
protected:
  Uint16 instanceCount;
  LFCraterInstance *instances;
  
  noise::module::RidgedMulti* pNoiseBase;
};