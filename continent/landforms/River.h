#pragma once
#include "../Landform.h"

#define LFRIVER_XOR 0x7ba3a428

#define LFRIVER_MAX_NODES 256

struct LFRiverNode {
  V2T<Uint16> pos;
  Uint8 width, flow;
};

struct LFRiverInstance {
  Uint16 nodeCount;
  LFRiverNode nodes[LFRIVER_MAX_NODES];
  bool hasFCoord;
  V2T<Uint16> fCoord;
  bool hasLake;
};

class River : public Landform {
public:
  River();
  ~River();
  
  void SetOwner(Continent *const n);
  
  Uint16 GetMaxInstances();
  void GenerateInstances();
  void DeleteInstances();
  
  void Save(std::ofstream *const fst);
  void Load(std::ifstream *const fst);
  
  //Generation triggers
  bool tPostBasic();
  bool tPostWind();
  void tDebugDraw(DebugImage *img);
  
  Uint32 GetXOR();
protected:
  Uint16 instanceCount;
  LFRiverInstance *instances;
  
  noise::module::RidgedMulti* pNoiseBase;
};