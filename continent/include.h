#pragma once
#include "shared/Shared.h"

class Continent;

namespace noise {
  namespace module {
    class Perlin;
    class Curve;
    class ScaledBase;
    class RidgedMulti;
    class ScaleBias;
    class Min;
    class Clamp;
    class Add;
    class Billow;
  };
};