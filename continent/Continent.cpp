#include "Continent.h"
#include "DebugImage.h"
#include "Console.h"
#include "Generator.h"
#include "Landform.h"
#include "Biome.h"

Continent::Continent() {
  overmap = NULL;
  omWidth = omHeight = 0;
  generator = new Generator(this);
  omTemp = NULL;
}
Continent::~Continent() {
  deleteOvermap();
  delete generator;
}

void Continent::SetSize(Uint16 nw, Uint16 nh) {
  width = nw;
  height = nh;
}

void Continent::SetOMSize(Uint16 nw, Uint16 nh) {
  if(overmap != NULL) deleteOvermap();
  omWidth = nw; omHeight = nh;
  ovWidth  = (Uint16)round(CONT_OV_BASE * ((float)nw / float(nh)) );
  ovHeight = (Uint16)round(CONT_OV_BASE * ((float)nh / float(nw)) );
  
  omMinLand = V2T<Uint16>(omWidth/17, omHeight/17);
  omMaxLand = V2T<Uint16>(omWidth-omMinLand.x, omHeight-omMinLand.y);
  allocateOvermap();
}

V2T<float> Continent::GetOMResolution() {//px per km
  return V2T<float>((float)omWidth / (float)width, (float)omHeight / (float)height);
}

V2T<float> Continent::GetOMScale() {//km per om
  return V2T<float>((float)width / (float)omWidth, (float)height / (float)omHeight);
}

V2T<float> Continent::GetOMMult() {//1/omDim
  return V2T<float>((float)1 / (float)omWidth, (float)1 / (float)omHeight);
}


V2T<float> Continent::GetGenScale() {
  return V2T<float>(((float)omWidth / 1024.f * 25.f), ((float)omHeight / 1024.f * 25.f));
}

Int16 Continent::SetBaseTempWind(V2T<Uint16> position) {
  //Interpolate 5-point data
  V2T<float> p = V2T<float>	(float(position.x) / float(omWidth ) * 2.f - 1.f, 
				 float(position.y) / float(omHeight) * 2.f - 1.f);
  WorldPixel *const sq = &overmap[position.x][position.y];
  
  const V2T<float> cpPos[5] = {
    V2T<float>(-1.f, -1.f), 
    V2T<float>( 1.f, -1.f),
    V2T<float>( 1.f,  1.f),
    V2T<float>(-1.f,  1.f),
    V2T<float>( 0.f,  0.f)
  };
  
  static float dists[CONT_TEMPSAMP_MAX]; const float sc = sqrt(2.f);
  dists[0] = 1.f - std::min(1.f, p.distanceTo(cpPos[0] / sc));
  dists[1] = 1.f - std::min(1.f, p.distanceTo(cpPos[1] / sc));
  dists[2] = 1.f - std::min(1.f, p.distanceTo(cpPos[2] / sc));
  dists[3] = 1.f - std::min(1.f, p.distanceTo(cpPos[3] / sc));
  dists[4] = 1.f - std::min(1.f, p.distanceTo(cpPos[4] / sc));
  
  float temp = 0.f, tot = 0.f;
  for(Uint8 i = 0; i < tempAvgCount; i++) {
    temp += dists[i] * tempAvgSamples[i];
    tot += dists[i];
  }
  temp /= tot;
  
  //Bias by elevation
  float plHeight = ((Int16)sq->elevation - (Int16)omSeaLevel - 16);
  if(plHeight < 0.f) plHeight *= .3f;
  
  float tempMinus = 2.78f * (plHeight / 16.f);
  temp -= tempMinus;
  
  //Calculate wind vectors (minimize temp via vector)
  tot = 0.f; float wX = 0.f, wY = 0.f;
  for(Uint8 i = 0; i < tempAvgCount; i++) {
    float dX = cpPos[i].x - p.x, dY = cpPos[i].y - p.y;
    float str = (tempAvgSamples[i] - temp);//Flow towards hotter/lower pressure
    tot += log2(abs(str))*sgn(str);
    wX += dX * str; wY += dY * str;
  }
  
  float distWind = sqrt(wX*wX + wY*wY);// * abs(tot));//Restrict to +-8 range
  wX /= distWind; wY /= distWind;//Max length vector
  wX *= WPWIND_VMULT; wY *= WPWIND_VMULT;
  
  sq->temp = C_WPT(temp);
  //sq->wind = WPWind{(Int8)round(wX), (Int8)round(wY)};
  
  const Uint16 vexelWidth = (omWidth / ovWidth), vexelHeight = (omHeight / ovHeight);
  if(position.x % vexelWidth == 0 && position.y % vexelHeight == 0 ) {//Set wind vexel
    WorldVexel *const v = &vexelmap[position.x / vexelWidth][position.y / vexelHeight];
    v->wind = WPWind{(Int8)round(wX), (Int8)round(wY)};
  }
  
  return C_WPT(temp);
}

bool Continent::GenerateOvermap() {
  //Create temporary structure
  omTemp = new WorldPixelTMP*[omWidth];
  for(Uint16 x = 0; x < omWidth; x++) {
    omTemp[x] = new WorldPixelTMP[omHeight];
  }
  
  //V2T<float> omRes = GetOMResolution();
  omOctaves = 18;//std::max(omRes.x, omRes.y) / 
  Landform::ActiveContinent(this);
  generator->Init();
  
  generator->GenerateBasics();
  Landform::AllPostBasic();
  generator->DerivativeMap();
  
  //Propagate moisture and alter wind vectors
  generator->BlurMoisture(3);
  generator->SimulateWind(1);
  generator->BlurMoisture(2);
  generator->MarkBasicFlags();
  
  Landform::AllPostWind();
  generator->DerivativeMap();
  
  generator->SimulateWind(1);
  generator->BlurMoisture(3);
  //Generate Biomes
  Biome::ActiveContinent(this);
  generator->ClassifyBiomes();
  //generator->BlurBiomes(3);
  
  
  for(Uint16 x = 0; x < omWidth; x++) {
    if(omTemp[x] != NULL) delete[] omTemp[x];
  }
  delete[] omTemp;
  omTemp = NULL;
  
  return true;
}

bool Continent::DebugOvermap(std::string path) {
  Console::Progress("[%p] Rendering overmap %e...", omWidth);
  DebugImage img(omWidth, omHeight);
  const register Uint8 seaLevel = omSeaLevel;
  for(int x = 0; x < omWidth; x++) {
    for(int y = 0; y < omHeight; y++) {
      WorldPixel *const p = &overmap[x][y];
      
      int drawElev = p->elevation; if(drawElev < seaLevel) drawElev = 0;
      int drawTemp = abs(WPT_C(p->temp)) * 3 + 127;
      int drawMoisture = p->moisture;
      
      int color = img.Color(drawTemp, drawElev, drawMoisture);
      
      
      img.Pixel(x, y, color);
    }
    Console::Progress(x);
  }
  
  int xVInc = omWidth / ovWidth*8, yVInc = omHeight / ovHeight*8;
  std::cout << "XVI: " << xVInc << ", " << yVInc << "\n\nOMD: " << omWidth << "/" << ovWidth << ", " << omHeight << "/" << ovHeight << "\n";;
  for(int x = 16; x < omWidth; x+=xVInc) {
    for(int y = 16; y < omHeight; y+=yVInc) {
      WorldVexel *const v = &vexelmap[x/xVInc*8][y/yVInc*8];
      int wColor = img.Color(255, 255, 255);
      const int eX = x + v->wind.x / WPWIND_DDIV, eY = y + v->wind.y / WPWIND_DDIV;
      
      img.Line(x, y, eX, eY, wColor);
      //img.Point(x, y, wColor);
    }
  }
  
  const int omDimBase = log2(std::max(omWidth, omHeight)) - 7;
  
  Landform::AllDebugDraw(&img);
  Console::Progress(omWidth);
  std::stringstream ss; ss << path << seed << "p" << omDimBase << ".png";
  img.Write(ss.str());
  Console::Print("Primary map saved to \"%s\"", ss.str()); ss.str("");
  
  //Secondary image
  for(int x = 0; x < omWidth; x++) {
    for(int y = 0; y < omHeight; y++) {
      WorldPixel *const p = &overmap[x][y];
      int color = img.Color((int)p->elevationDev * (127/WPELDE_SCALE) + 127, p->dxde + 128, p->dyde + 128);
      img.Pixel(x, y, color);
    }
  }
  ss << path << seed << "s" << omDimBase << ".png";
  img.Write(ss.str());
  Console::Print("Secondary map saved to \"%s\"", ss.str()); ss.str("");
  
  //Biome map
  for(int x = 0; x < omWidth; x++) {
    for(int y = 0; y < omHeight; y++) {
      WorldPixel *const p = &overmap[x][y];
      Color c = Color(0,0,0);
      if(p->waterState == WPWS_OCEAN) c = Color(0,0, 254);
      else if(p->waterState == WPWS_LAKE) c = Color(0, 128, 254);
      
      float tBlend = 0.001f;
      for(Uint8 i = 0; i < WPBIOME_BLENDCNT; i++) { 
	tBlend += p->biome[i].blend;
      }
      //tBlend = 31.f;
      
      for(Uint8 i = 0; i < WPBIOME_BLENDCNT; i++) { 
	Biome *const biome = Biome::Get(p->biome[i].id);
	if(biome == NULL) break;
	float str = clamp((p->biome[i].blend) /(tBlend), 0.f, 1.f);///tBlend;
	c.r += biome->mapColor.r * str;
	c.g += biome->mapColor.g * str;
	c.b += biome->mapColor.b * str;
      }
      //c.r = p->biome[0].blend * (256.f/(WPBIOME_BSTRM+1.f));
      //c.g = p->biome[1].blend * (256.f/(WPBIOME_BSTRM+1.f));
      int color = img.Color(c.r, c.g, c.b);
      img.Pixel(x, y, color);
    }
  }
  ss << path << seed << "b" << omDimBase << ".png";
  img.Write(ss.str());
  Console::Print("Biome map saved to \"%s\"", ss.str()); ss.str("");
  
  return true;
}

struct COMCoordData {
  float weights[4];
  Uint16 baseX, baseY;
};

static inline COMCoordData getOMWeights(float x, float y, Uint16 width, Uint16 height, Uint16 ovWidth, Uint16 ovHeight) {
  Uint16 ovBaseX = (Uint16)floor((x / (double)width ) * (double)ovWidth );
  Uint16 ovBaseY = (Uint16)floor((y / (double)height) * (double)ovHeight);
  const float ovFracX = float(((x / (double)width ) * (double)ovWidth ) - (double)ovBaseX);
  const float ovFracY = float(((y / (double)height) * (double)ovHeight) - (double)ovBaseY);
  const float invFracX = 1.f - ovFracX, invFracY = 1.f - ovFracY;
  
  COMCoordData ret; //base pxby pxpy bxpy
  ret.weights[0] = ovFracX  * ovFracY ;
  ret.weights[1] = invFracX * ovFracY ;
  ret.weights[3] = invFracX * invFracY;
  ret.weights[2] = ovFracX  * invFracY;
  
  ret.baseX = ovBaseX;
  ret.baseY = ovBaseY;
  
  return ret;
}

WorldPixel Continent::GetOMPixel(float x, float y) {
  WorldPixel ret;
  COMCoordData cd = getOMWeights(x, y, width, height, omWidth, omHeight);
  
  float elevation = 0.f, moisture = 0.f, aura = 0.f, temp = 0.f, elevationDev = 0.f, dxde = 0.f, dyde = 0.f;
  Uint8 flags;
  for(int i = 0; i < 4; i++) {
    Uint16 px = i % 2, py = i / 2;
    const WorldPixel *const p = &overmap[cd.baseX + px][cd.baseY + py];
    elevation	+= cd.weights[i] * p->elevation;
    moisture	+= cd.weights[i] * p->moisture;
    aura	+= cd.weights[i] * p->aura;
    temp	+= cd.weights[i] * p->temp;
    elevationDev+= cd.weights[i] * p->elevationDev;
    dxde	+= cd.weights[i] * p->dxde;
    dyde	+= cd.weights[i] * p->dyde;
    
    flags |= p->flags;
  }
  
  ret.elevation	 = (Uint8)clamp((int)round(elevation), 0, 255);
  ret.moisture	 = (Uint8)clamp((int)round(moisture ), 0, 255);
  ret.aura	 = (Uint8)clamp((int)round(aura     ), 0, 255);
  
  ret.temp		= (Int16)round(temp);
  ret.elevationDev 	= (Int8)round(elevationDev);
  ret.dxde = (Int8)round(dxde);
  ret.dyde = (Int8)round(dyde);
  
  return ret;
}

WorldVexel Continent::GetOMVexel(float x, float y) {
  WorldVexel ret;
  COMCoordData cd = getOMWeights(x, y, width, height, ovWidth, ovHeight);
  if(cd.baseX > ovWidth || cd.baseY > ovHeight) {
    std::cout << x << ", " << y << " -> " << cd.baseX << ", " << cd.baseY << "\n";
  }
  
  float wpwX = 0.f, wpwY = 0.f;//WPWind data
  for(int i = 0; i < 4; i++) {
    Uint16 px = i % 2, py = i / 2;
    const WorldVexel *const v = &vexelmap[cd.baseX + px][cd.baseY + py];
    wpwX += cd.weights[i] * v->wind.x;
    wpwY += cd.weights[i] * v->wind.y;
  }
  
  ret.wind.x = (Int8)clamp((int)round(wpwX), -7, 7);
  ret.wind.y = (Int8)clamp((int)round(wpwY), -7, 7);
  
  return ret;
}

bool Continent::SaveBase(std::string path) {
  Console::Print("Saving base data...");
  std::stringstream ss; ss << path << seed << "b.gcnt";
  
  std::ofstream fst(ss.str(), std::ofstream::binary);
  const std::string vLabel = "0.10a VDT\n";
  fst.write(vLabel.c_str(), 10);
  fst.write((char*)&seed, sizeof(Uint64));
  fst.write((char*)&width, sizeof(Uint16)*(4 + 2*2));//width, height, omWidth, omHeight
  fst.write((char*)&omVerticalScale, sizeof(float));
  fst.write((char*)&omAvgElevation, sizeof(Uint8));
  
  fst.write((char*)&omSeaLevel, sizeof(Uint8));
  fst.write((char*)&omOctaves, sizeof(int));
  
  fst.write((char*)&tempAvgCount, sizeof(Uint8));
  for(Uint8 i = 0; i < tempAvgCount; i++) {
    fst.write((char*)&tempAvgSamples[i], sizeof(float));
  }
  
  //Write map data
  for(int x = 0; x < omWidth; x++) {
    for(int y = 0; y < omHeight; y++) {
      WorldPixel *const p = &overmap[x][y];
      fst.write((const char*)p, sizeof(WorldPixel));
    }
  }
  
  fst.close();
  
  Console::Print("Base data was saved to \"%s\"", ss.str()); ss.str("");
  return true;
}

#define CLB_CHKERR if(fst.fail()) { Console::Error("Unable to load data from path!"); return false; }
bool Continent::LoadBase(std::string path) {
  std::stringstream ss; ss << "Loading base data from \"" << path << "\"";
  Console::Print(ss.str()); ss.str("");
  
  ss << path << seed << "b.gcnt";
  std::ifstream fst(ss.str(), std::ofstream::binary); CLB_CHKERR;
  char vString[10];
  fst.read(vString, 10);
  fst.read((char*)&seed, sizeof(Uint64));
  fst.read((char*)&width, sizeof(Uint16)*(4 + 2*2));//width, height, omWidth, omHeight
  fst.read((char*)&omVerticalScale, sizeof(float));
  fst.read((char*)&omAvgElevation, sizeof(Uint8));
  CLB_CHKERR;
  
  fst.read((char*)&omSeaLevel, sizeof(Uint8));
  fst.read((char*)&omOctaves, sizeof(int));
  CLB_CHKERR;
  
  this->SetSize(width, height);//256
  this->SetOMSize(omWidth, omHeight);
  
  fst.read((char*)&tempAvgCount, sizeof(Uint8));
  for(Uint8 i = 0; i < tempAvgCount; i++) {
    fst.read((char*)&tempAvgSamples[i], sizeof(float));
  }
  CLB_CHKERR;
  
  fst.close();
  return true;
}

void Continent::ClearTempFlags() {
  for(int x = 0; x < omWidth; x++) {
    for(int y = 0; y < omHeight; y++) {
      WorldPixel *const p = &overmap[x][y];
      p->processed = false;
      p->ffID = 0;
    }
  }
}

void Continent::allocateOvermap() {
  if(overmap != NULL) deleteOvermap();
  overmap = new WorldPixel*[omWidth];
  for(Uint16 x = 0; x < omWidth; x++) {
    overmap[x] = new WorldPixel[omHeight];
  }
  
  vexelmap = new WorldVexel*[ovWidth];
  for(Uint16 x = 0; x < ovWidth; x++) {
    vexelmap[x] = new WorldVexel[ovHeight];
  }
}

void Continent::deleteOvermap() {
  if(overmap == NULL) return;
  
  for(Uint16 x = 0; x < omWidth; x++) {
    if(overmap[x] != NULL) delete[] overmap[x];
  }
  delete[] overmap;
  
  for(Uint16 x = 0; x < ovWidth; x++) {
    if(vexelmap[x] != NULL) delete[] vexelmap[x];
  }
  delete[] vexelmap;
  
  if(omTemp != NULL) {
    for(Uint16 x = 0; x < omWidth; x++) {
      if(omTemp[x] != NULL) delete[] omTemp[x];
    }
    delete[] omTemp;
    omTemp = NULL;
  }
}