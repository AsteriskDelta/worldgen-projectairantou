#include <iostream>
#include "continent/Continent.h"
#include "continent/Landform.h"
#include "continent/Biome.h"
#include "lib/Console.h"

#define W_SEED Uint32(0xa7D326B5)

#include <sys/resource.h>
static void incStackSize() {
  const rlim_t kStackSize = 64 * 1024 * 1024;   // min stack size = 64 MB
  struct rlimit rl; int result = getrlimit(RLIMIT_STACK, &rl);
  if (result == 0) {
    if (rl.rlim_cur < kStackSize) {
      rl.rlim_cur = kStackSize;
      result = setrlimit(RLIMIT_STACK, &rl);
      if (result != 0) {
	fprintf(stderr, "setrlimit returned result = %d\n", result);
      }
    }
  }
}

//WorldGenObject WorldGen;


int main(int argc, char** argv) {
  _unused(argc); _unused(argv);
  incStackSize();
  
  Console::SetIOOutput(true);
  
  Landform::LoadAll();
  Biome::LoadAll();
  
  srand(time(NULL));
  for(int i = 9; i < 13; i++) {
    Continent *island = new Continent;
    island->SetSize(312, 312);//256
    island->SetOMSize(pow(2, i), pow(2, i));//(1024, 1024);
    island->seed = 311438882;//rand();//24266355;//rand();//867638724//1111213116
    island->omSeaLevel = 64;
    island->omVerticalScale = .01f;//1/100 KM per PX
    
    island->tempAvgSamples[0] = float(-9); island->tempAvgSamples[1] = float(-4);
    island->tempAvgSamples[2] = float(31); island->tempAvgSamples[3] = float(29);
    island->tempAvgSamples[4] = float(22);//Center
    island->tempAvgCount = 5;
    
    island->GenerateOvermap();
    island->DebugOvermap("./");
    if(i == 0) island->SaveBase("./");
    delete island;
  }
  
  /*WorldGenData world;
  world.width = world.height = 2048;
  world.delta = 0.4f; world.initial = 0.8f;
  world.subdivisions = 6;
  world.propagation = 4;
  world.startPoints = 8;
  world.seed = rand();
  world.variance = 32;
  world.scale = 25.0f * ((float)world.height / 1024.0f);
  
  world.aTemp = 680;
  world.hTempX = 0; world.hTempY = 0;
  world.hTemp = 1020;

  world.lTempX = world.width; world.lTempY = world.height;
  world.lTemp = -180;
  
  WorldGen.Generate(world);*/
  return 0;
}